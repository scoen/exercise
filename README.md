# Agile Software Engineering UI5 Training

## Preparation for Exercises

### Software to be installed before the training

- Have **Google Chrome** installed and updated to latest version
- Have **git** installed : https://git-scm.com/downloads
- Have **Node.js** *(LTS version)* installed : https://nodejs.org/en/download/
  - It also installs [**NPM**](https://www.npmjs.com/)
- Have [**UI5 tooling**](https://www.npmjs.com/package/@ui5/cli) installed globally : `npm install -g @ui5/cli`
- Have [**UIVeri5**](https://github.com/SAP/ui5-uiveri5) installed globally : `npm install @ui5/uiveri5 -g`
- Have [**Yeoman**](https://yeoman.io/) and [**generator-easy-ui5**](https://github.com/SAP/generator-easy-ui5) installed globally: `npm install -g yo generator-easy-ui5`
- Have **Visual Studio Code** installed with the following extensions:
  - ESLint: Search for `dbaeumer.vscode-eslint`
  - Live Share: Search for `ms-vsliveshare.vsliveshare`

### Exercises repository

To get the exercises, please clone the exercise repository:

`git clone https://github.tools.sap/ASE-UI5/ASEUI5TrainingExercises.git`

Hint: Do not clone the repository to a synchronized folder like `OneDrive`.

If you need to provide username and password, use the following:

- username: Your User-ID (D- or I-number).
- password: A personal access token from your Github account. If you don't have a personal access token yet, you can generate it [here](https://github.tools.sap/settings/tokens).

> If you face issues with cloning the repository from GitHub ensure that sslVerify is set to false : `git config --global http.sslVerify false`  
> If that does not solve the issue in your case let the trainers know **before** the training.

Once the repository is cloned, execute `npm install` or `npm ci` ([what's the difference?](https://docs.npmjs.com/cli/v7/commands/npm-ci)) in the `ASEUI5TrainingExercise` folder.

### Visual Studio Code

If you are not yet using the **Visual Studio Code**, you should install it before the training, as you use it during the training.
You can download it at [https://code.visualstudio.com/](https://code.visualstudio.com/) and pick the **recommended stable build** version for your operating system.

**Recommendation:** In the VS Code settings (`ctrl + ,`), set `Auto Save` to `afterDelay`. With that, any editor will get saved after 1 second.

For more information on local UI5 development in VS Code, have a look at the [guide](https://pages.github.tools.sap/EngineeringCulture/ase/SAPUI5/ui5WithVscode?rc=1) or the [COP UI5 recording by Boris Sachsenberg](https://web.microsoftstream.com/video/ea639932-f340-46b2-a85d-78a0aa0a3c15).

### Zoom Client for Meetings *(if Zoom is used)*

#### New installation

The web browser client will download automatically when you start or join your first Zoom meeting, and is also available for manual download here:
<https://zoom.us/client/latest/ZoomInstaller.exe>

#### Update

Manually check for updates by clicking your profile icon in the top right corner and selecting "Check for Updates".

### MS Teams Client *(if MS Teams is used)*

Ensure that MS Teams is available and working properly.

#### Breakout rooms

The trainers will ask you to join a **specific breakout room** during the exercise. It is important that **before** joining the room you **enable the notifications** on the main classroom. This way, the trainer will be able to communicate messages while you are doing the exercises.

### Mural *(if Mural is used)*

- During the training, open the Mural link provided by the trainers.
- Sign in with your E-Mail address.
- Enter your name in the list of Participants and how long you have already worked with UI5.

   > Doubleclick, e.g. on the green Post-It to enter a name. Technically the Post-It will be duplicated. Make sure that the Post-It is more or less placed over the original position.
   If you have issues with Mural write us a mail.

- Add Post-Its to the Expectations area.

### Slack *(if Slack is used)*

- Download and install Slack from [slack.com/downloads](https://slack.com/intl/en-in/downloads).
- Sign into your workspace, e.g. **sap-btp**.slack.com
- Add the following Channel to your workspace:
  - **#sap-tech-ui5-testing**

## Software Skills

In order to complete all the exercises in the training, you should bring:

- Basic app development skills
  - [SAPUI5 Demo Kit](https://sapui5.hana.ondemand.com/)
  - [Walkthrough Tutorial](https://sapui5.hana.ondemand.com/#/topic/3da5f4be63264db99f2e5b04c5e853db)
  - [Worklist Tutorial](https://sapui5.hana.ondemand.com/#/topic/6a6a621c978145ad9eef2b221d2cf21d)
- JavaScript programming skills & OO knowledge
- Basic git skills
  - [Modules 4 - 6 of SAP IBSO Tools Bootcamp](https://github.wdf.sap.corp/pages/ibso-tools-bootcamp/coursematerial/virtual-eng/)
- Commitment to apply agile practices consistently
- Motivated to coach and sustain best practices in teams
- Ability to channel solutions through network of experts

## Trouble shooting

In case you have done all the node.js installation above and `npm start` does not work on your Windows PC, check your system variables.
In the windows search, type sy and open "Edit the system environment variables". Select "Environment Variables" and double click Path. The correct setting is
`C:\Users\<YourUser>\AppData\Roaming\npm`.

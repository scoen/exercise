sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"sap/ui/test/matchers/Properties",
	"sap/ui/test/actions/Press",
	"sap/ui/test/matchers/BindingPath",
	"sap/ui/test/matchers/AggregationFilled"
], function (
	Opa5,
	PropertyStrictEquals,
	Properties,
	Press,
	BindingPath,
	AggregationFilled) {
	"use strict";

	Opa5.createPageObjects({
		onTheCategory : {
			viewName: "Category",

			actions: {
				iPressOnTheProductBlasterExtreme: function () {
					this.waitFor({
						controlType: "sap.m.ObjectListItem",
						matchers: new Properties({title : "Blaster Extreme"}),
						actions: new Press(),
						errorMessage: "The product Blaster Extreme was not found and could not be pressed"
					});
				},

				iPressTheBackButtonInCategory: function () {
					return this.waitFor({
						id: "page",
						actions: new Press(),
						errorMessage: "The nav back button was not displayed"
					});
				},

				iPressOnCompareLink: function (ProductId) {
					return this.waitFor({
						controlType: "sap.m.ObjectAttribute",
						matchers: [
							new BindingPath({path: "/Products('" + ProductId + "')"}),
							new Properties({text: "Compare"})
						],
						actions: new Press(),
						errorMessage: "The product list does not contain required selection " + ProductId
					});
				},

				iPressOnTheFirstProduct: function () {
					return this.waitFor({
						controlType: "sap.m.ObjectListItem",
						matchers: new BindingPath({path: "/Products('HT-1254')"}),
						actions: new Press(),
						errorMessage: "The product list does not contain required selection"
					});
				}
			},

			assertions: {
				iShouldBeTakenToTheSpeakerCategory: function () {
					return this.waitFor({
						controlType: "sap.m.Page",
						matchers: new PropertyStrictEquals({name: "title", value: "Speakers"}),
						success: function (aPage) {
							Opa5.assert.ok(
								aPage,
								"The speaker category page was found"
							);
						},
						errorMessage: "The speaker category page was not found"
					});
				},

				iShouldSeeCompareLinkOnListEntry: function () {
					this.waitFor({
						controlType: "sap.m.ObjectAttribute",
						matchers: new Properties({text : "Compare"}),
						success: function () {
							Opa5.assert.ok(true, "List entry has an compare link");
						},
						errorMessage: "List entry has no compare link"
					});
				},

				iShouldBeTakenToTheFlatScreensCategory: function () {
					return this.waitFor({
						controlType: "sap.m.Page",
						matchers: new PropertyStrictEquals({name: "title", value: "Flat Screens"}),
						success: function (aPage) {
							Opa5.assert.ok(
								aPage,
								"The flat screens category page was found"
							);
						},
						errorMessage: "The flat screens category page was not found"
					});
				},

				iShouldSeeTheProductList: function () {
					return this.waitFor({
						id: "productList",
						success: function (oList) {
							Opa5.assert.ok(
								oList,
								"The product list was found"
							);
						},
						errorMessage: "The product list was not found"
					});
				},

				iShouldSeeSomeEntriesInTheProductList: function () {
					this.waitFor({
						id: "productList",
						matchers: new AggregationFilled({name: "items"}),
						success: function (oList) {
							Opa5.assert.ok(
								oList.getItems().length > 0,
								"The product list has entries"
							);
						},
						errorMessage: "The product list does not contain any entries"
					});
				}
			}
		}
	});
});

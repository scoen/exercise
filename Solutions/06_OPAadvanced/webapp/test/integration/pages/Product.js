sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"sap/ui/test/actions/Press"
], function (
	Opa5,
	PropertyStrictEquals,
	Press) {
	"use strict";

	Opa5.createPageObjects({
		onTheProduct: {
			viewName: "Product",

			actions: {
				iAddTheDisplayedProductToTheCart: function () {
					return this.waitFor({
						controlType: "sap.m.Button",
						matchers: new PropertyStrictEquals({name: "text", value: "Add to Cart"}),
						actions : new Press(),
						errorMessage: "The press action could not be executed"
					});
				},

				iToggleTheCart: function () {
					return this.waitFor({
						controlType: "sap.m.Button",
						matchers: new PropertyStrictEquals({name: "icon", value: "sap-icon://cart"}),
						actions: new Press(),
						errorMessage: "The cart button was not found and could not be pressed"
					});
				}
			},

			assertions: {
				iShouldSeeTheBlasterExtremeDetailPage: function () {
					return this.waitFor({
						success: function () {
							Opa5.assert.ok(true, "The Blaster Extreme page was successfully displayed");
						},
						errorMessage: "The Blaster Extreme page was not displayed"
					});
				}
			}
		}
	});
});

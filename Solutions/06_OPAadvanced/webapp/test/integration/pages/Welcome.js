sap.ui.define([
	"sap/ui/test/Opa5"
], function (
	Opa5) {
	"use strict";

	Opa5.createPageObjects({
		onTheWelcomePage: {
			viewName: "Welcome",

			actions: {
			},

			assertions: {
				iShouldSeeTheWelcomePage: function () {
					return this.waitFor({
						success: function () {
							Opa5.assert.ok(true, "The welcome page was successfully displayed");
						},
						errorMessage: "The welcome page was not displayed"
					});
				}
			}
		}
	});

});

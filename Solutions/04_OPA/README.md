# OPA Solution

1. Navigate into the integration folder under test

2. ...

3. NavigationJourney.js :

```
opaTest(<myMessage>, function (Given, When, Then) {
	// Arrangements
	Given.iStartMyApp();
	// Actions
	When.<myPage>.<myTestAction()>;
	// Assertions
	Then.<myPage>.<myTestAssertion()>;
});
```

4. ...

5. home.js :

```
sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/matchers/Properties"
], function (
	Opa5,
	Press,
	Properties) {
	"use strict";

	Opa5.createPageObjects({
		onTheHomePage: {
			actions: {
				iPressOnTheSpeakerCategory: function () {
					return this.waitFor({
						controlType: "sap.m.StandardListItem",
						matchers: new Properties({
							title: "Speakers"
						}),
						actions: new Press(),
						errorMessage: "The category list does not contain required selection"
					});
				}
			},

			assertions: {
				iShouldSeeTheCategoryList: function () {
					return this.waitFor({
						id: "categoryList",
						success: function (oList) {
							Opa5.assert.ok(oList, "Found the category List");
						}
					});
				}
			}
		}
	});
});
```

5. category.js :

```
sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/matchers/Properties"
], function (
	Opa5,
	Press,
	Properties) {
	"use strict";

	Opa5.createPageObjects({
		onTheCategoryPage: {
			actions: {
		
			},

			assertions: {
				iShouldSeeTheCategoryList: function () {
					return this.waitFor({
						id: "categoryList",
						success: function (oList) {
							Opa5.assert.ok(oList, "Found the category List");
						}
					});
				}
			}
		}
	});
});
```

**Tipp:** If you need any more ideas and help, check out the [testing tutorial](https://sapui5.hana.ondemand.com/#/topic/1b47457cbe4941ee926317d827517acb). 

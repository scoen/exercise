sap.ui.define([
	"./BaseController",
	"../model/formatter"
], function (
	BaseController,
	formatter) {
	"use strict";

	return BaseController.extend("sap.ui.demo.cart.controller.Product", {
		formatter: formatter,

		onInit: function () {
			const oComponent = this.getOwnerComponent();
			this._router = oComponent.getRouter();
			this._router.getRoute("product").attachPatternMatched(this._routePatternMatched, this);

			this._router.getTarget("product").attachDisplay(function (oEvent) {
				this.fnUpdateProduct(oEvent.getParameter("data").productId);// update the binding based on products cart selection
			}, this);
		},

		_routePatternMatched: function (oEvent) {
			const sId = oEvent.getParameter("arguments").productId;
				const oView = this.getView();
				const oModel = oView.getModel();
			// the binding should be done after insuring that the metadata is loaded successfully
			oModel.metadataLoaded().then(function () {
				const sPath = "/" + this.getModel().createKey("Products", {
						ProductId: sId
					});
				oView.bindElement({
					path: sPath,
					events: {
						dataRequested: function () {
							oView.setBusy(true);
						},
						dataReceived: function () {
							oView.setBusy(false);
						}
					}
				});
				const oData = oModel.getData(sPath);
				//if there is no data the model has to request new data
				if (!oData) {
					oView.setBusyIndicatorDelay(0);
					oView.getElementBinding().attachEventOnce("dataReceived", function () {
						// reset to default
						oView.setBusyIndicatorDelay(null);
						this._checkIfProductAvailable(sPath);
					}.bind(this));
				}
			}.bind(this));
		},

		fnUpdateProduct: function (productId) {
			const sPath = "/Products('" + productId + "')";
				const fnCheck = function () {
					this._checkIfProductAvailable(sPath);
				};

			this.getView().bindElement({
				path: sPath,
				events: {
					change: fnCheck.bind(this)
				}
			});
		},

		_checkIfProductAvailable: function (sPath) {
			const oModel = this.getModel();
			const oData = oModel.getData(sPath);

			// show not found page
			if (!oData) {
				this._router.getTargets().display("notFound");
			}
		},

		/**
		 * Navigate to the generic cart view
		 * @param {sap.ui.base.Event} @param oEvent the button press event
		 */
		onToggleCart: function (oEvent) {
			const bPressed = oEvent.getParameter("pressed");
			const oEntry = this.getView().getBindingContext().getObject();

			this._setLayout(bPressed ? "Three" : "Two");
			this.getRouter().navTo(bPressed ? "productCart" : "product", {
				id: oEntry.Category,
				productId: oEntry.ProductId
			});
		}
	});
});

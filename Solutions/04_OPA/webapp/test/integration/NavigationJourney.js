sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/Home",
	"./pages/Category",
	"./pages/Product",
	"./pages/Welcome"
], function (opaTest) {
	"use strict";

	QUnit.module("Navigation Journey");

	opaTest("Should start the app and go to the speaker category view", function (Given, When, Then) {
		// Arrangements
		Given.iStartMyApp();
		// Actions
		When.onTheHomePage.iPressOnTheSpeakerCategory();
		// Assertions
		Then.onTheCategoryPage.iShouldBeTakenToTheSpeakerCategory();
	});

	opaTest("Should see the product Blaster Extreme", function (Given, When, Then) {
		// Actions
		When.onTheCategoryPage.iPressOnTheProductBlasterExtreme();
		// Assertions
		Then.onTheProductPage.iShouldSeeTheBlasterExtremeDetailPage();
	});

	opaTest("Should navigate back to home", function (Given, When, Then) {
		// Actions
		When.onTheCategoryPage.iPressTheBackButtonInCategory();
		// Assertions
		Then.onTheHomePage.iShouldSeeTheCategoryList();
		Then.onTheWelcomePage.iShouldSeeTheWelcomePage();
		// Cleanup
		Then.iTeardownMyApp();
	});

});

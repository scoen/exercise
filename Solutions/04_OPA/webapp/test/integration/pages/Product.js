sap.ui.define([
	"sap/ui/test/Opa5"
], function (
	Opa5) {
	"use strict";

	Opa5.createPageObjects({
		onTheProductPage: {
			viewName: "Product",

			actions: {
			},

			assertions: {
				iShouldSeeTheBlasterExtremeDetailPage: function () {
					return this.waitFor({
						success: function () {
							Opa5.assert.ok(true, "The Blaster Extreme page was successfully displayed");
						},
						errorMessage: "The Blaster Extreme page was not displayed"
					});
				}
			}
		}
	});
});

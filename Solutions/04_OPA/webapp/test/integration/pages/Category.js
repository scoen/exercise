sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"sap/ui/test/matchers/Properties",
	"sap/ui/test/actions/Press"
], function (
	Opa5,
	PropertyStrictEquals,
	Properties,
	Press) {
	"use strict";

	Opa5.createPageObjects({
		onTheCategoryPage: {
			viewName: "Category",

			actions: {
				iPressOnTheProductBlasterExtreme: function () {
					this.waitFor({
						controlType: "sap.m.ObjectListItem",
						matchers: new Properties({title: "Blaster Extreme"}),
						actions: new Press(),
						errorMessage: "The product Blaster Extreme was not found and could not be pressed"
					});
				},

				iPressTheBackButtonInCategory: function () {
					return this.waitFor({
						id: "page",
						actions: new Press(),
						errorMessage: "The nav back button was not displayed"
					});
				}
			},

			assertions: {
				iShouldBeTakenToTheSpeakerCategory: function () {
					return this.waitFor({
						controlType: "sap.m.Page",
						matchers: new PropertyStrictEquals({name: "title", value: "Speakers"}),
						success: function (aPage) {
							Opa5.assert.ok(
								aPage,
								"The speaker category page was found"
							);
						},
						errorMessage: "The speaker category page was not found"
					});
				}
			}
		}
	});
});

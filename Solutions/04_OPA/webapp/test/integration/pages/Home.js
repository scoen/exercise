sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/matchers/Properties"
], function (
	Opa5,
	Press,
	Properties) {
	"use strict";

	Opa5.createPageObjects({
		onTheHomePage: {
			viewName: "Home",
			actions: {
				iPressOnTheSpeakerCategory: function () {
					return this.waitFor({
						controlType: "sap.m.StandardListItem",
						matchers: new Properties({
							title: "Speakers"
						}),
						actions: new Press(),
						errorMessage: "The category list does not contain required selection"
					});
				}
			},

			assertions: {
				iShouldSeeTheCategoryList: function () {
					return this.waitFor({
						id: "categoryList",
						success: function (oList) {
							Opa5.assert.ok(oList, "Found the category List");
						}
					});
				}
			}
		}
	});
});

/* eslint-disable qunit/require-expect */
sap.ui.define([
	"sap/ui/demo/cart/model/models"
], function (models) {
	"use strict";

	QUnit.module("createDeviceModel", {
		afterEach: function () {
			this.oDeviceModel.destroy();
		}
	});

	const fnIsPhoneTestCase = function (assert, bIsPhone) {
		// Arrange
		// eslint-disable-next-line ui5/no-global-name
		this.stub(sap.ui.Device, "system", { phone: bIsPhone });

		// System under test
		this.oDeviceModel = models.createDeviceModel();

		// Assert

		assert.strictEqual(this.oDeviceModel.getProperty("/system/phone"), bIsPhone, "IsPhone property is correct");
	};

	QUnit.test("Should initialize a device model for desktop", function (assert) {
		fnIsPhoneTestCase.call(this, assert, false);
	});

	QUnit.test("Should initialize a device model for phone", function (assert) {
		fnIsPhoneTestCase.call(this, assert, true);
	});

	const fnIsTouchTestCase = function (assert, bIsTouch) {
		// Arrange
		// eslint-disable-next-line ui5/no-global-name
		this.stub(sap.ui.Device, "support", { touch: bIsTouch });

		// System under test
		this.oDeviceModel = models.createDeviceModel();

		// Assert
		assert.strictEqual(this.oDeviceModel.getProperty("/support/touch"), bIsTouch, "IsTouch property is correct");
	};

	QUnit.test("Should initialize a device model for non touch devices", function (assert) {
		fnIsTouchTestCase.call(this, assert, false);
	});

	QUnit.test("Should initialize a device model for touch devices", function (assert) {
		fnIsTouchTestCase.call(this, assert, true);
	});

	QUnit.test("The binding mode of the device model should be one way", function (assert) {

		// System under test
		this.oDeviceModel = models.createDeviceModel();

		// Assert
		assert.strictEqual(this.oDeviceModel.getDefaultBindingMode(), "OneWay", "Binding mode is correct");
	});


});

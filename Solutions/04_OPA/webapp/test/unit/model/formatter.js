/* eslint-disable qunit/require-expect */
sap.ui.define([
	"sap/ui/demo/cart/model/formatter",
	"../helper/FakeI18nModel"
], function (formatter, FakeI18nModel) {
	"use strict";

	QUnit.module("price");

	const fnPriceTestCase = function (assert, sValue, fExpectedNumber) {
		// Act
		const fNumber = formatter.price(sValue);

		// Assert
		assert.strictEqual(fNumber, fExpectedNumber, "The formatting was correct");
	};

	QUnit.test("Should format a number with no digits", function (assert) {
		fnPriceTestCase.call(this, assert, "123", "123,00");
	});

	QUnit.test("Should contain a decimal separator for large numbers", function (assert) {
		fnPriceTestCase.call(this, assert, "12345.67", "12.345,67");
	});

	QUnit.test("Should round a number with more than 2 digits", function (assert) {
		fnPriceTestCase.call(this, assert, "3.123", "3,12");
	});

	QUnit.test("Should format a negative number properly", function (assert) {
		fnPriceTestCase.call(this, assert, "-3", "-3,00");
	});

	QUnit.test("Should format an empty string properly", function (assert) {
		fnPriceTestCase.call(this, assert, "", "0,00");
	});

	QUnit.test("Should format a zero properly", function (assert) {
		fnPriceTestCase.call(this, assert, "0", "0,00");
	});

	QUnit.module("totalPrice");

	const fnTotalPriceTestCase = function (assert, oProducts, sExpectedText) {

		//Act
		const oControllerStub = {
			getResourceBundle: function () {
				return new FakeI18nModel({
					"cartTotalPrice": "Foo: {0}"
				}).getResourceBundle();
			}
		};
		const fnStubbedFormatter = formatter.totalPrice.bind(oControllerStub);
		const sText = fnStubbedFormatter(oProducts);

		//Assert
		assert.strictEqual(sText, sExpectedText, "Correct total text was assigned");
	};

	QUnit.test("Should multiply the price with the quantity for  1 product", function (assert) {
		const oProducts = {
			1: {Price: 123, Quantity: 2}
		};
		fnTotalPriceTestCase.call(this, assert, oProducts, "Foo: 246,00");
	});

	QUnit.test("Should format a quantity of 0 to a total of zero for one product", function (assert) {
		const oProducts = {
			1: {Price: 123, Quantity: 0}
		};
		fnTotalPriceTestCase.call(this, assert, oProducts, "Foo: 0,00");
	});

	QUnit.test("Should format two products with quantities and digits to the correct price", function (assert) {
		const oProducts = {
			1: {Price: 123.45, Quantity: 1},
			2: {Price: 456.78, Quantity: 2}
		};
		fnTotalPriceTestCase.call(this, assert, oProducts, "Foo: 1.037,01");
	});

	QUnit.module("statusText");
	const fnStatusTextTestCase = function (assert, sStatus, sExpectedText) {

		//Act
		const oControllerStub = {
			getResourceBundle: function () {
				return new FakeI18nModel({
					"statusA": "1",
					"statusO": "2",
					"statusD": "3"
				}).getResourceBundle();
			}
		};
		const fnStubbedFormatter = formatter.statusText.bind(oControllerStub);
		const sText = fnStubbedFormatter(sStatus);

		//Assert
		assert.strictEqual(sText, sExpectedText, "Correct text was assigned");
	};

	QUnit.test("Should provide the status text 'statusA' for products with status A", function (assert) {
		fnStatusTextTestCase.call(this, assert, "A", "1");
	});

	QUnit.test("Should provide the status text 'statusO' for products with status O", function (assert) {
		fnStatusTextTestCase.call(this, assert, "O", "2");
	});

	QUnit.test("Should provide the status text 'statusD' for products with status D", function (assert) {
		fnStatusTextTestCase.call(this, assert, "D", "3");
	});

	QUnit.test("Should provide the original input for all other values", function (assert) {
		fnStatusTextTestCase.call(this, assert, "foo", "foo");
		fnStatusTextTestCase.call(this, assert, "", "");
	});

	QUnit.module("statusState");
	const fnStatusStateTestCase = function (assert, sStatus, sExpectedState) {
		//Act
		const sState = formatter.statusState(sStatus);

		//Assert
		assert.strictEqual(sState, sExpectedState, "The formatter returned the correct state");
	};

	QUnit.test("Should return \"Success\" status for products with status A", function (assert) {
		fnStatusStateTestCase.call(this, assert, "A", "Success");
	});
	QUnit.test("Should return \"Warning\" status for products with status A", function (assert) {
		fnStatusStateTestCase.call(this, assert, "O", "Warning");
	});
	QUnit.test("Should return \"Error\" status for products with status A", function (assert) {
		fnStatusStateTestCase.call(this, assert, "D", "Error");
	});
	QUnit.test("Should return \"None\" status for all other statuses", function (assert) {
		fnStatusStateTestCase.call(this, assert, "foo", "None");
		fnStatusStateTestCase.call(this, assert, "", "None");
	});

	QUnit.module("pictureUrl");

	QUnit.test("Should return the url to a product picture relative to the app's root directory", function (assert) {

		// Act
		const sResult = formatter.pictureUrl("sap/ui/demo/mock/images/foo.jpg");

		//Assert
		assert.strictEqual(sResult, "https://sapui5.hana.ondemand.com/test-resources/sap/ui/documentation/sdk/images/foo.jpg", "The formatter returned the correct URL");
	});

	QUnit.module("footerTextForCart");

	const fnFooterTextForCartTestCase = function (assert, oProducts, sExpectedText) {
		//Act
		const oControllerStub = {
			getResourceBundle: function () {
				return new FakeI18nModel({
					"cartSavedForLaterFooterText": "1"
				}).getResourceBundle();
			}
		};
		const fnStubbedFormatter = formatter.footerTextForCart.bind(oControllerStub);
		const sText = fnStubbedFormatter(oProducts);

		//Assert
		assert.strictEqual(sText, sExpectedText, "Correct total text was assigned");
	};

	QUnit.test("Should return \"\" for no products", function (assert) {
		fnFooterTextForCartTestCase.call(this, assert, {}, "");
	});

	QUnit.test("Should return \"cartSavedForLaterFooterText\" for products", function (assert) {
		fnFooterTextForCartTestCase.call(this, assert, {1: "foo"}, "1");
	});
});

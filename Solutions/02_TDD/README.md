# Solution - Test Driven Development with Javascript and QUnit

Develop a feature to format the price of a product using test-driven development.

* Go to the **Solutions/02_TDD** folder.
* In `webapp/test/unit/model/formatter.js` you find the unit tests that were developed step-by-step in the module that tests the function `formatter.price`
* A possible solution for the productive code you find in `webapp/model/formatter.js` -> `formatter.price`

## Hints

* By executing `npm run test:watch` in the exercise root folder, you can use the karma test runner to keep your tests watched.
* Structure the test for `formatter.price` in a new `QUnit.module` to keep the overview.
* In the SAPUI5 SDK you get all necessary details about [**sap.ui.core.format.NumberFormat**](https://sapui5.hana.ondemand.com/#/api/sap.ui.core.format.NumberFormat)

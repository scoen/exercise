sap.ui.define(["sap/ui/demo/cart/model/formatter"], function (formatter) {
	"use strict";

	QUnit.module("formatter - statusText", (hooks) => {

		const sDefaultLanguage = sap.ui.getCore().getConfiguration().getLanguage();

		hooks.before(function () {
			sap.ui.getCore().getConfiguration().setLanguage("en-US");
		});

		hooks.after(function () {
			sap.ui.getCore().getConfiguration().setLanguage(sDefaultLanguage);
		});

		QUnit.test("Should see statusText 'Available'", function (assert) {
			assert.equal(formatter.statusText("A"), "Available", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Out of Stock'", function (assert) {
			assert.equal(formatter.statusText("O"), "Out of Stock", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Discontinued'", function (assert) {
			assert.equal(formatter.statusText("D"), "Discontinued", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Other'", function (assert) {
			assert.equal(formatter.statusText("Other"), "Other", "Status Text is correct");
		});

	});

	QUnit.module("TDD exercise: Implement formatter.price", (hooks) => {

		const sDefaultLanguage = sap.ui.getCore().getConfiguration().getLanguage();

		hooks.before(function () {
			sap.ui.getCore().getConfiguration().setLanguage("en-US");
		});

		hooks.after(function () {
			sap.ui.getCore().getConfiguration().setLanguage(sDefaultLanguage);
		});

		QUnit.test("Price should return 42.00", function (assert) {
			assert.equal(formatter.price(42), "42.00", "42.00 is returned");
		});

		QUnit.test("Price should return 21.00", function (assert) {
			assert.equal(formatter.price(21), "21.00", "21.00 is returned");
		});

		QUnit.test("Price should return error if paramter is NaN", function (assert) {
			assert.equal(formatter.price("Text"), "Parameter is not a number", "Error is displayed if parameter is NaN");
		});

		QUnit.test("Price should correctly handle decimal numbers", function (assert) {
			assert.equal(formatter.price(42.21), "42.21", "Decimal number is formatted with two digits after dot");
		});

		QUnit.test("Price should return a number . as grouping seperator", function (assert) {
			assert.equal(formatter.price(1042), "1,042.00", "Number is formatted with grouping seperator");
		});
	});
});

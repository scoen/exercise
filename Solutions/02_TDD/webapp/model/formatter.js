sap.ui.define([
	"sap/ui/demo/cart/model/ResourceLoader",
	"sap/ui/core/format/NumberFormat"
], function (resourceLoader, NumberFormat) {
	"use strict";

	return {
		/**
		 * Returns the status text based on the product status
		 * @param {string} sStatus product status
		 * @return {string} the corresponding text if found or the original value
		 */
		statusText: function (sStatus) {
			const oBundle = resourceLoader.i18nBundle();
			const mStatusText = {
				"A": oBundle.getText("statusA"),
				"O": oBundle.getText("statusO"),
				"D": oBundle.getText("statusD")
			};

			return mStatusText[sStatus] || sStatus;
		},

		price: function (number){
			if (!isNaN(number)) {
				const oNumberFormat = NumberFormat.getFloatInstance({
					decimals: 2
				});
				return oNumberFormat.format(number);
			}
			return "Parameter is not a number";
		}
	};
});

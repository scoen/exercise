sap.ui.define(["sap/ui/model/resource/ResourceModel"], (ResourceModel) => {
    "use strict";

    return {
        getResourceBundle(file) {
            const oBundle = new ResourceModel({
				// eslint-disable-next-line ui5/no-global-name
				bundleUrl: sap.ui.require.toUrl("sap/ui/demo/cart/") + file
            }).getResourceBundle();
            return oBundle;
        },

        i18nBundle() {
            return this.getResourceBundle("i18n/i18n.properties");
        }
    };
});

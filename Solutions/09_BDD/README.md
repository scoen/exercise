# Gherkin Exercise

## Steps

1. Navigate into your test/integration folder
2. Now create a new file \<YourFeatureName\>.feature
3. The structure of this file should look like this:

```
Feature: Save a product for later

  Background:
    Given I start my App
    When on home: I press on "The Flat Screens category"
    When on the category: I press on "The first Product"
    When on the product: I add the displayed product to the cart
    When on the product: I toggle the cart
    When on the cart: I press on save for later for the first product

  Scenario: Save for later
    Then on the cart: I should see one product in my save for later list
    Then on the cart: I should see an empty cart

...

 ```

4. Add all page objects you need for this feature to the configureOpa.js file

```javascript
sap.ui.define([
	"sap/ui/test/Opa5",
	"./arrangements/Startup",
	// Page Objects
	"./pages/Home",
...
], function (Opa5, Startup) {
	"use strict";

	Opa5.extendConfig({
		arrangements : new Startup(),
		viewNamespace : "sap.ui.demo.cart.view.",
		autoWait: true
	});
});
```

5. Add the name of your feature file to the opaTests.qunit.js file
(Tip: You could also reference multiple features here.)

```javascript
/* global QUnit */

QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/gherkin/opa5TestHarness",
	"sap/ui/demo/cart/test/integration/configureOpa"
], function (testHarness) {
	"use strict";

	testHarness.test({
		featurePath: "sap/ui/demo/cart/test/integration/SaveForLater",
		generateMissingSteps : true
	});

	QUnit.start();
});
```
6.  Run the tests:
    1.  Open a terminal in the exercise root folder.
    2.   Open the project structure in your browser `ui5 serve -o`
    3.   Go into `webapp/test/integration` subfolder and open `opaTests.qunit.html`
    4.   Alternatively use karma testrunner `npm run test:watch` in exercise root folder.

## Optional Exercise: Add a steps file

7. Add a steps.js in test/integration folder, e.g.:

```javascript
sap.ui.define([
  "sap/ui/test/gherkin/StepDefinitions",
  "sap/ui/test/Opa5",
  "sap/ui/test/actions/Press"
], function (StepDefinitions, Opa5, Press) {
  "use strict";

  return StepDefinitions.extend("GherkinWithOPA5.Steps", {
    init: function () {
      var oOpa5 = new Opa5();
      this.register(/^I press on (.*)$/i, function (sProductName) {
        oOpa5.waitFor({
          controlType: "sap.m.ObjectListItem",
          viewName: "Category",
          bindingPath: {
            path: `/Products('${sProductName}')`,
            propertyPath: "PictureUrl"
          },
          actions: new Press({
            idSuffix: "content"
          })
        });
      });
    }
  });
});
```

You may change the parameters of `this.register()` according to your needs. <br>
Be careful: You can overwrite the existing mappings to the page objects since OPA5 only generates them if there is no matching step definition!

8. Extend the opaTests.qunit.js by importing `configureOpa` and your new `steps.js` and adding it to the testHarness:

```javascript
sap.ui.require([
	"sap/ui/test/gherkin/opa5TestHarness",
	"sap/ui/demo/cart/test/integration/configureOpa",
	"sap/ui/demo/cart/test/integration/steps"
], function (testHarness, configureOpa, stepsFile) {
	"use strict";

	testHarness.test({
		featurePath: "<Reference to feature file>",
		generateMissingSteps : true,
		steps:  stepsFile
	});

	QUnit.start();
});
```

9. Adjust your .feature file to use the steps.js instead of a page object. In this example you can use the productname as a parameter:

```
Feature: Save a product for later

  Background:
    Given I start my App
    When on home: I press on "The Flat Screens category"
    When I press on HT-1254
    When on the product: I add the displayed product to the cart
    When on the product: I toggle the cart
    When on the cart: I press on save for later for the first product

  Scenario: Save for later
    Then on the cart: I should see one product in my save for later list
    Then on the cart: I should see an empty cart

	...
 ```

 10. You can also use the already defined page objects and provide them with parameters. To do that, add a second `this.register()` block to the `init` function your steps.js, e.g.:

```javascript
this.register(/^on the cartview I should see (.*?) product(?:s)? in my save for later list$/i,
        // Hint: Given and When are needed here even if they are not used. 
        // Otherwise "Then" can not be interpreted
        function(sValue, Given, When, Then) {
            Then.onTheCart.iShouldSeeProductsInMySaveForLaterList(sValue);
        });
```

11. Add the corresponding step to your .feature file, e.g.

```
Scenario: Save for later
    Then on the cartview I should see 1 product in my save for later list
```

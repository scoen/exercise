sap.ui.define([
  "sap/ui/test/gherkin/StepDefinitions",
  "sap/ui/test/Opa5",
  "sap/ui/test/actions/Press"
], function (StepDefinitions, Opa5, Press) {
  "use strict";

  return StepDefinitions.extend("GherkinWithOPA5.Steps", {
    init: function () {
      var oOpa5 = new Opa5();
      this.register(/^I press on (.*)$/i, function (sProductName) {
        oOpa5.waitFor({
          controlType: "sap.m.ObjectListItem",
          viewName: "Category",
          bindingPath: {
            path: `/Products('${sProductName}')`,
            propertyPath: "PictureUrl"
          },
          actions: new Press({
            idSuffix: "content"
          })
        });
      });

      this.register(/^on the cartview I should see (.*?) product(?:s)? in my save for later list$/i,
        // Hint: Given and When are needed here even if they are not used. 
        // Otherwise "Then" can not be interpreted
        function(sValue, Given, When, Then) {
            Then.onTheCart.iShouldSeeProductsInMySaveForLaterList(sValue);
        });
    }

  });

});
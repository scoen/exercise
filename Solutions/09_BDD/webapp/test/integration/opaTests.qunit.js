/* global QUnit */

QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/gherkin/opa5TestHarness",
	"sap/ui/demo/cart/test/integration/configureOpa",
	"sap/ui/demo/cart/test/integration/steps/steps"
], function (testHarness, configureOpa, stepsFile) {
	"use strict";

	testHarness.test({
		featurePath: "sap/ui/demo/cart/test/integration/features/SaveForLater",
		generateMissingSteps : true,
		steps:  stepsFile
	});

	QUnit.start();
});

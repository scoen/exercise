# Solution - Unit-Tests with QUnit

## Prerequisites

- Clone this[repository](../../../..) into your local IDE workspace.

## Instructions

Write unit tests for the app *Shopping Cart* to format the status text of a product with the following steps:

1. Go to the **Exercises/01_QUNIT** folder.
2. Create a testsuite in webapp/test/unit/
   > Due to CSP compliance you should create an html and a js-file.

	Snippet to load and initialize UI5 (bootstraping)

	**unitTests.qunit.html**

	```html
	<script
		id="sap-ui-bootstrap"
		src="https://sapui5.hana.ondemand.com/resources/sap-ui-core.js"
		data-sap-ui-resourceroots='{
			"sap.ui.demo.cart": "../../"
		}'
		data-sap-ui-async="true">
	</script>
	})),
	```

	Refer to script file:
	```javascript
	<script src="unitTests.qunit.js"></script>
	```

	**unitTests.qunit.js**
	```javascript
	/* global QUnit */
	QUnit.config.autostart = false;
	
	sap.ui.getCore().attachInit(function () {
		"use strict";
	
		sap.ui.require([
			"sap/ui/demo/cart/test/unit/AllTests"
		], function () {
			QUnit.start();
		});
	});
	```

3. Create a subfolder for your formatter tests. Use test structure should match the app structure to easily find the corresponding unit tests.

	```javascript
	├── test
		├── unit
			├── model
	```

4. Create the new formatter QUnit Test.
5. In the test, refer to the formatter class in your productive code.

   ```javascript
   sap.ui.define(['sap/ui/demo/cart/model/formatter'], function(formatter) {
	"use strict";
   ```

6. Write your first test that challenges the formatter.statusText.

   ```javascript
   QUnit.test("Should see statusText 'Available'", function(assert) {
		assert.equal(formatter.statusText('A'), "Available", "Status Text is correct");
	});
   ```

7. Create the **AllTests.js** file and refer the new QUnit Test:

   **AllTests.js**

   ```javascript
    sap.ui.define([
		"./model/formatter"
	], function () {
		"use strict";
	});
   ```

8.  Run the tests:
    1. Open a terminal in the exercise root folder.
    1. Open the project structure in your browser by running `ui5 serve -o`
    1. Go into `webapp/test/unit` subfolder and open `unitTests.qunit.html`
    1. Alternatively use the script `npm start` and open `unitTests.qunit.html`
    1. You may also use the karma testrunner `npm run test:watch` in the exercise root folder.

9. Add more tests. Check the coverage report to see if you covered all the lines and branches.

   ```javascript
  	QUnit.test("Should see statusText 'Out of Stock'", function(assert) {
		assert.equal(formatter.statusText('O'), "Out of Stock", "Status Text is correct");
	});
	
	QUnit.test("Should see statusText 'Discontinued'", function(assert) {
		assert.equal(formatter.statusText('D'), "Discontinued", "Status Text is correct");
	});
	
	QUnit.test("Should see statusText 'Other'", function(assert) {
		assert.equal(formatter.statusText('Other'), "Other", "Status Text is correct");
	});
   ```

10. Use eslint and pre-commit hooks:
    1. In the terminal, run `npm run eslint`. See if any errors or warnings are reported.
    1. Fix the errors and warnings. If you have the eslint plugin for VS Code installed, try using the shortcut `ctrl + .` for quick fixes.
    1. Check the [package.json in the root directory of the repository](../../package.json) for the `"ghooks"` configuration. [Ghooks](https://github.com/ghooks-org/ghooks) is a plugin that automatically sets up git hooks, e.g. the `pre-commit hook` before `git commit`.
    1. Add a script similar to `lint-test-qunit-solution` in the ghooks `pre-commit` hook to automatically run the test in the qunit exercise while committing. If you use `npm test` this will also check on the coverage thresholds defined in the `karma.conf.js`.
    1. Commit your changes to see it working.

## Optional Exercise

11. Make yourself familiar with different assertions. You can find them in the [QUnit Documentation](https://api.qunitjs.com/assert/).

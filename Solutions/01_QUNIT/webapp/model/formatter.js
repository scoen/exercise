sap.ui.define([
	"sap/ui/demo/cart/model/ResourceLoader"
], function (resourceLoader) {
	"use strict";

	return {
		/**
		 * Returns the status text based on the product status
		 * @param {string} sStatus product status
		 * @return {string} the corresponding text if found or the original value
		 */
		statusText: function (sStatus) {
			const oBundle = resourceLoader.i18nBundle();
			const mStatusText = {
				"A": oBundle.getText("statusA"),
				"O": oBundle.getText("statusO"),
				"D": oBundle.getText("statusD")
			};

			return mStatusText[sStatus] || sStatus;
		}
	};
});

sap.ui.define(["sap/ui/demo/cart/model/formatter"], function (formatter) {
	"use strict";

	QUnit.module("formatter - statusText", function (hooks) {

		hooks.beforeEach(function () {
			// empty by intention
		});

		hooks.afterEach(function () {
			// empty by intention
		});

		QUnit.test("Should see statusText 'Available'", function (assert) {
			assert.equal(formatter.statusText("A"), "Available", "Status Text is correct");
		});
		QUnit.test("Should see statusText 'Out of Stock'", function (assert) {
			assert.equal(formatter.statusText("O"), "Out of Stock", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Discontinued'", function (assert) {
			assert.equal(formatter.statusText("D"), "Discontinued", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Other'", function (assert) {
			assert.equal(formatter.statusText("Other"), "Other", "Status Text is correct");
		});

	});

	QUnit.module("Examples for different assertions", () => {

		QUnit.test("Should check equal, strictEqual and deepEqual", (assert) => {
			assert.equal(1, true, "1 should be converted to true");
			assert.notEqual(1, false, "1 should be converted to true");
			assert.notStrictEqual(1, true, "1 and true is not strict equal");
			assert.notEqual({}, {}, "Different objects are not equal");
			assert.deepEqual({}, {}, "Objects are deep equal");
			assert.notDeepEqual({solution: "42"}, {solution: "43"}, "Objects are not deep equal");
		});

		QUnit.test("Should check for truthy", (assert) => {
			assert.ok(true, "true is truthy");
			assert.notOk(false, "false is not truthy");
			assert.ok({}, "empty object is truthy");
		});

		QUnit.test("Should check if method throws exception", (assert) => {
			assert.throws(() => {
				throw new Error("Exception");
			}, "Should throw an exception");
		});

		QUnit.test("Match regular expressions", (assert) => {
			assert.ok("String to match".match(/match/), "The word 'match' was contained in the string!");
			assert.notOk("String to mathc".match(/match/), "The word 'match' was not contained in the string!");
			assert.deepEqual("A sentence with the letter E".match(/e/g), ["e", "e", "e", "e", "e", "e"], "The letter 'e' was contained 6 times in the sentence!");
		});
	});
});

# Solution - Run your App with the Mock Server

## Instructions

1. Create the **initMockServer.js** file in your webapp/test folder with the following content:
```
sap.ui.define([
	"sap/ui/demo/cart/localService/mockserver",
	"sap/m/MessageBox"
], function (mockserver, MessageBox) {
	"use strict";

	// initialize the mock server
	mockserver.init().catch(function (oError) {
		MessageBox.error(oError.message);
	}).finally(function () {
		// initialize the embedded component on the HTML page
		sap.ui.require(["sap/ui/core/ComponentSupport"]);
	});
});
```
2. Create the **mockServer.html** page in your webapp/test folder with the following content:
```
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Shopping Cart</title>

	<script
		id='sap-ui-bootstrap'
		src='https://sapui5.hana.ondemand.com/resources/sap-ui-core.js'
		data-sap-ui-theme='sap_fiori_3'
		data-sap-ui-compatVersion="edge"
		data-sap-ui-async="true"
		data-sap-ui-oninit="module:<Reference to initMockServer>"
		data-sap-ui-resourceroots='{
			"sap.ui.demo.cart" : "../",
			"sap.ui.demo.mock": "https://sapui5.hana.ondemand.com/test-resources/sap/ui/documentation/sdk/"
		}'>
	</script>

</head>
<body class='sapUiBody' id='content'>
	<div data-sap-ui-component data-name="sap.ui.demo.cart" data-id="container" data-settings='{"id" : "cart"}'></div>
</body>
</html>
```
3. Fill in your app path in *localService/mockserver.js*: _sAppPath = "sap/ui/demo/cart/"
4. Extend mock data for the app *Shopping Cart* by...
  - Adding a new product category
   
  **localService/mockdata/ProductCategories.json**
  ```
  {
	"Category": "TA",
	"CategoryName": "Training Accessories",
	"NumberOfProducts": 1
  }
  ```
  - Adding several products to this product category
   
  **localService/mockdata/Products.json**
  ```
  {
	"ProductId": "TA-1000",
	"Category": "TA",
	"Name": "Flipchart",
	"ShortDescription": "Flipchart",
	"SupplierName": "Ultrasonic United",
	"Weight": 10,
	"WeightUnit": "KG",
	"Price": 90,
	"Status": "A",
	"CurrencyCode": "EUR",
	"DimensionWidth": 69,
	"DimensionDepth": 5,
	"DimensionHeight":  98,
	"PictureUrl": "sap/ui/demo/mock/images/HT-1258.jpg"
  }
  ```
5. Call and Mock a Function Import
  - Add a new button in the footer of the Product view.
  - Assign the function **onReserveProduct** from the **BaseController** to the button.
   
  **view/Product.view.xml**
  ```
  <Button
	text="{i18n>reserveProduct}"
	type="Emphasized"
	press=".onReserveProduct" />
  ```
  > The function **onReserveProduct** calls a function import that returns *success: true* if the product could be reserved.
  - Add a custom mock behaviour for function import **ReserveProduct**:
    - Respond with **status OK**, a suitable **content type** for a JSON object and a string representation of the result
    - The result should be either *success: true* if *productId=HT-2001* is passed to the function import
    - ... or *success: false* if *productId=HT-2026* or *productId=HT-2026* is passed to the function import
    
    **localService/mockserver.js**
    ```
    // custom mock behaviour may be added here
	aRequests.push({
		method: "GET",
		path: new RegExp("ReserveProduct.?productId=HT-2001"),
		response: function (oXhr) {
			Log.debug("Incoming request for ReserveProduct");
			oXhr.respond(200, {
				"Content-Type": "application/json;charset=utf-8"
			}, JSON.stringify({
				d: {
					success: true
				}
			}));
			return true; // Skip default processing
		}
	});
    aRequests.push({
		method: "GET",
		path: new RegExp("ReserveProduct.?(productId=HT-2000|productId=HT-2026)"),
		response: function (oXhr) {
			Log.debug("Incoming request for ReserveProduct");
			oXhr.respond(200, {
				"Content-Type": "application/json;charset=utf-8"
			}, JSON.stringify({
				d: {
					success: false
				}
			}));
			return true; // Skip default processing
		}
	});
    ```

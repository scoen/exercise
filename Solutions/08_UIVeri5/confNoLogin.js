exports.config = {	
	profile : 'integration',
	baseUrl: 'https://sapui5.hana.ondemand.com/test-resources/sap/m/demokit/cart/webapp/index.html?sap-ui-theme=sap_fiori_3',	 
	timeouts: {
		getPageTimeout: '20000',
		allScriptsTimeout: '22000',
		defaultTimeoutInterval: '60000'
	},
	reporters : [
	{
		name : './reporter/junitReporter',
		reportName: './target/report/report.xml'
	}],
	browsers:[{
		browserName: 'chromeHeadless',
		capabilities: {
			remoteWebDriverOptions: {
				browserSize: {
					width: 1920,
					height: 1067
			}
		  }
		}
	}]
};

module.exports = createPageObjects({
	ProductDetail: {
		actions: {
			iAddProductToCart: function() {
				element(by.control({
					controlType: "sap.m.Button",
					viewName: "sap.ui.demo.cart.view.Product",
					i18NText: {
						propertyName: "text",
						key: "addToCartShort"
					}
				})).click();
			},
			iShowShoppingCart: function() {
				element(by.control({
					controlType: "sap.m.ToggleButton",
					viewName: "sap.ui.demo.cart.view.Product",
					i18NText: {
						propertyName: "tooltip",
						key: "toCartButtonTooltip"
					}
				})).click();
			}
		},

		assertions: {
			iShouldSeeProduct: function () {
				var product = element(by.control({
					controlType: "sap.m.ObjectHeader",
					viewName: "sap.ui.demo.cart.view.Product",
					bindingPath: {
						path: "/Products('HT-1040')",
						propertyPath: "Name"
					}
				}));
				expect(product.isPresent()).toBeTruthy();
			}
		}
	}
});
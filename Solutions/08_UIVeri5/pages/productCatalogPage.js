module.exports = createPageObjects({
	ProductCatalog: {
		actions: {
			iSelectProductCategory: function (sProductCategory) {
				element(by.control({
					controlType: "sap.m.StandardListItem",
					viewName: "sap.ui.demo.cart.view.Home",
					bindingPath: {
						path: "/ProductCategories('PR')",
						propertyPath: "CategoryName"
					}
				})).click();
			},
			iSelectProduct: function () {
				element(by.control({
					controlType: "sap.m.ObjectListItem",
					viewName: "sap.ui.demo.cart.view.Category",
					bindingPath: {
						path: "/Products('HT-1040')"
					}
				})).click();
			}
		},

		assertions: {

		}
	}
});
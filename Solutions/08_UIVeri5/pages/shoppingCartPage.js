module.exports = createPageObjects({
	ShoppingCart: {
		assertions: {
			iShouldSeeProduct: function () {		
				var product = element(by.control({
					controlType: "sap.m.ObjectListItem",
					viewName: "sap.ui.demo.cart.view.Cart",
					bindingPath: {
						path: "/cartEntries/HT-1040",
						propertyPath: "Quantity",
						modelName: "cartProducts"
					}
				}));		 
				expect(product.isPresent()).toBeTruthy();
			}
		}
	}
});
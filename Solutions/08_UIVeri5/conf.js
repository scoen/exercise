exports.config = {	
	profile : 'integration',
	baseUrl: 'https://uiveri5demo.internal.cfapps.sap.hana.ondemand.com/index.html',
	auth: {
		"sapcloud-form": {
			user: "${params.user}",
			pass: "${params.pass}",
			userFieldSelector: 'input[name="username"]',
			passFieldSelector: 'input[name="password"]',
			logonButtonSelector: 'input[type="submit"]'
		}
	},   
	timeouts: {
		getPageTimeout: '20000',
		allScriptsTimeout: '22000',
		defaultTimeoutInterval: '60000'
	},
	reporters : [
	{
		name : './reporter/junitReporter',
		reportName: './target/report/report.xml'
	}],
	browsers:[{
		browserName: 'chromeHeadless',
		capabilities: {
			remoteWebDriverOptions: {
				browserSize: {
					width: 1920,
					height: 1067
			}
		  }
		}
	}]
};

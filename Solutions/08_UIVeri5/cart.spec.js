var productCataloguePage = require('./pages/productCatalogPage'),
    productDetailPage = require('./pages/productDetailPage'),
    shoppingCartPage = require('./pages/shoppingCartPage');    

describe('test', function () {
    it("Select a product in category Printers", function () {
        When.onTheProductCatalogPage.iSelectProductCategory();
        When.onTheProductCatalogPage.iSelectProduct();
        Then.onTheProductDetailPage.iShouldSeeProduct();
    });

    it("Add selected product to cart and check that it is available in the shopping cart", function () {
        When.onTheProductDetailPage.iAddProductToCart();
        When.onTheProductDetailPage.iShowShoppingCart();
        Then.onTheShoppingCartPage.iShouldSeeProduct();
    });
});
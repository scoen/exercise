# 08 - UIVeri5 Scenario and System-Test

**Deployed Shopping Cart on internal CF account:**

<https://uiveri5demo.internal.cfapps.sap.hana.ondemand.com/index.html>

> User: uiveri5demo@mailinator.com  
> PW: UIveri5demo

**UIVeri5 GitHub:**

<https://github.com/SAP/ui5-uiveri5>

## Project Structure

The project structure would look like below:

```
├── Shopping Cart
│   ├── Pages
│       ├── productCatalogPage.js
│       ├── productDetailPage.js
│       ├── ShoppingCartPage.js
│   ├── target
│       ├── report
|   ├── conf.js
|   ├── cart.spec.js
```

## Hints:

* Add all locators, actions and assertions in page object.

* Add all test steps in spec.js

* Add all mandatory configuration settings like baseUrl and authentication in conf.js.

* You can pass username and password as parameters from the command line via the following:
  
```javascript
auth: {
   "sapcloud-form": {
      user: "${params.user}",
      pass: "${params.pass}",
      userFieldSelector: 'input[name="username"]',
      passFieldSelector: 'input[name="password"]',
      logonButtonSelector: 'input[type="submit"]',
   }
}, 
```

## Steps to write first UIVeri5 test:

1. Write system tests for the app *Shopping Cart* with the following steps:

   1. Click on the Categories "Printers".

      Snippet to locate a category by using BindingPath

      ```javascript
      var category = element(by.control({
      		controlType: "sap.m.StandardListItem",
      		bindingPath: {path: "/ProductCategories('PR')"}
      })),
      ```

      Snippet to trigger an action

      ```javascript
      iSelectProductCategory: function (sProductCategory) {
         category.click();
      },
      ```

   2. Select one Product e.g. "Laser Professional Eco" in catalog categories (like in step1)

   3. Verify the product is displayed on the right side in details view.

      snippet to locate a product

      ```javascript
      product = element(by.control({
      		controlType: "sap.m.ObjectListItem",
      		bindingPath: {path: "/Products('HT-1040')"}
      	}));
      ```

      snippet for assertion

      ```javascript
      assertions: {
      	iShouldSeeProduct: function () {
      		expect(product.isPresent()).toBeTruthy();
      	}
      }
      ```

   4. Click on "Add to Cart button".

      snippet to locate a button using I18nText

      ```javascript
      var addToCartButton = element(by.control({
      	controlType: "sap.m.Button",
      	I18NText: { propertyName: "text", key: "addToCartShort" }
      })),
      ```

   5. Click on "Show Shopping Cart" button.

   6. Verify that the product is added in the shopping cart.

## Execute UIVeri5 tests

* Commandline: `uiveri5 --params.user=uiveri5demo@mailinator.com  --params.pass=UIveri5demo`
* Without global UIVeri5:
  * `npm run uiveri5 --  --params.user=uiveri5demo@mailinator.com  --params.pass=UIveri5demo`
  * `uiveri5` needs to be added as a devDependency and script to `package.json`.
  * `--` passes arguments to npm script.
* Enable verbose mode: -v

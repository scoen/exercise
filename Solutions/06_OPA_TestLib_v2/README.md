# Fiori Elements Test with OPA5 Solution

## Code snippet for a List Journey

```javascript
opaTest("Starting the app and search", function (Given, When, Then) {
	Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display_toggleOff"});
   When.onTheGenericListReport.iExecuteTheSearch();
   Then.onTheGenericListReport.theResultListIsVisible();
});

opaTest("Should see the extension popup", function (Given, When, Then) {
   When.onTheGenericListReport.iSelectListItemsByLineNo([0]);
   When.onTheGenericListReport.iClickTheButtonWithId("customAction");
   Then.onTheGenericListReport.iShouldSeeTheDialogWithTitle("Show Details");
   Then.onTheListReportPage.iShouldSeeSalesOrderId("500000030");
});

opaTest("Should see the extension popup", function (Given, When, Then) {
   When.onTheGenericListReport.iClickTheButtonOnTheDialog("Close");
   Then.onTheGenericListReport.theResultListIsVisible();
   Then.iLeaveMyFLPApp();
});
```

## Code Snippet for own Page Object assertion

e.g. by using the test recorder and replacing the binding path with properties / text.

```javascript
iShouldSeeSalesOrderId: function(sSalesOrderId) {
   this.waitFor({
      controlType: "sap.m.Text",
      viewName: "sap.suite.ui.generic.template.ListReport.view.ListReport",
      properties: {text: sSalesOrderId},
      searchOpenDialogs: true
   });
}
```

## Code Snippet for activated feature toggle

```javascript
opaTest("Starting the app and search", function (Given, When, Then) {
   Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display_toggleOn", mockdataPath: "/mockdataToggleOn"});
   When.onTheGenericListReport.iExecuteTheSearch();
   Then.onTheGenericListReport.theAvailableNumberOfItemsIsCorrect(3);
});

opaTest("Should see the extension popup with the new title", function (Given, When, Then) {
   When.onTheGenericListReport.iSelectListItemsByLineNo([0]);
   When.onTheGenericListReport.iClickTheButtonWithId("customAction");
   Then.onTheGenericListReport.iShouldSeeTheDialogWithTitle("New Details Dialog Title");
});

opaTest("Should close the extension popup", function (Given, When, Then) {
   When.onTheGenericListReport.iClickTheButtonOnTheDialog("Close");
   Then.onTheGenericListReport.theResultListIsVisible();
   Then.iLeaveMyFLPApp();
});
```

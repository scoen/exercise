sap.ui.define([
	"sap/ui/test/Opa5",
	"./arrangements/Startup",
	"./arrangements/FLP",
	"./ListReportJourney",
	"./ListReportWithNewDetailsDialogJourney",
	"sap/suite/ui/generic/template/integration/testLibrary/ListReport/pages/ListReport"
], function (Opa5, Startup, FLP) {
	"use strict";

	Opa5.extendConfig({
		arrangements: new Startup(),
		assertions: new FLP(),
		viewNamespace: "STTASOWD20.view.",
		autoWait: true,
		testLibs: {
			fioriElementsTestLibrary: {
				Common: {
					appId: "STTASOWD20",
					entitySet: "C_STTA_SalesOrder_WD_20"
				}
			}
		}
	});
});

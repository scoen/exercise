sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/ListReport"
], function (opaTest) {
		"use strict";
		QUnit.module("ListReport Journey");

		// opaTest("Starting the app and search", function (Given, When, Then) {
		// 	Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display"});
		// 	When.onTheListReportPage.iClickTheGoButton();
		// 	Then.onTheListReportPage.theSmartTableIsFilled();
		// 	Then.iLeaveMyFLPApp();
		// });

		opaTest("Starting the app and search", function (Given, When, Then) {
			Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display"});
			When.onTheGenericListReport.iExecuteTheSearch();
			Then.onTheGenericListReport.theAvailableNumberOfItemsIsCorrect(3);
		});

		opaTest("Should see the extension popup", function (Given, When, Then) {
			When.onTheGenericListReport.iSelectListItemsByLineNo([0]);
			When.onTheGenericListReport.iClickTheButtonWithId("customAction");
			Then.onTheGenericListReport.iShouldSeeTheDialogWithTitle("Show Details");
			Then.onTheListReportPage.iShouldSeeSalesOrderId("500000030");
		});

		opaTest("Should close the extension popup", function (Given, When, Then) {
			When.onTheGenericListReport.iClickTheButtonOnTheDialog("Close");
			Then.onTheGenericListReport.theResultListIsVisible();
			Then.iLeaveMyFLPApp();
		});
	}
);

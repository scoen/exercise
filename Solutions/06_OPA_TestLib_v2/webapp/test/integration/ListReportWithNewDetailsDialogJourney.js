sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/ListReport"
], function (opaTest) {
		"use strict";
		QUnit.module("ListReport with new Details Dialog Journey");

		opaTest("Starting the app and search", function (Given, When, Then) {
			Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display", mockdataPath: "/mockdataToggleOn"});
			When.onTheGenericListReport.iExecuteTheSearch();
			Then.onTheGenericListReport.theAvailableNumberOfItemsIsCorrect(3);
		});

		opaTest("Should see the extension popup with the new title", function (Given, When, Then) {
			When.onTheGenericListReport.iSelectListItemsByLineNo([0]);
			When.onTheGenericListReport.iClickTheButtonWithId("customAction");
			Then.onTheGenericListReport.iShouldSeeTheDialogWithTitle("New Details Dialog Title");
		});

		opaTest("Should close the extension popup", function (Given, When, Then) {
			When.onTheGenericListReport.iClickTheButtonOnTheDialog("Close");
			Then.onTheGenericListReport.theResultListIsVisible();
			Then.iLeaveMyFLPApp();
		});
	}
);

sap.ui.define([
	"../localService/C_STTA_SalesOrder_WD_20/mockserver",
	"../localService/featureToggleService/mockserver",
	"./flpSandbox",
	"sap/ui/fl/FakeLrepConnectorLocalStorage",
	"sap/m/MessageBox"
], function (defaultMockserver, featureToggleMockServer, flpSandbox, FakeLrepConnectorLocalStorage, MessageBox) {
	"use strict";

	const aInitializations = [];

	// initialize the mock servers
	aInitializations.push(defaultMockserver.init());
	aInitializations.push(flpSandbox.init());
	aInitializations.push(featureToggleMockServer.init({mockdataPath: "/mockdataToggleOn"}));

	Promise.all(aInitializations).catch(function (oError) {
		MessageBox.error(oError.message);
	}).finally(function () {
		FakeLrepConnectorLocalStorage.enableFakeConnector();
	});
});

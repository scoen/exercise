sap.ui.define([
	"STTASOWD20/ext/service/FeatureToggleService",
	"sap/ui/thirdparty/sinon-4"
], function (FeatureToggleService, sinon) {
	"use strict";

	QUnit.module("Evaluate feature toggle", function (hooks) {

		let oSandbox = null;
		let oGetFeatureStatusStub = null;
		let oController = null;

		hooks.before(() => {
			oSandbox = sinon.sandbox.create();
		});

		hooks.beforeEach(() => {
			oController = sap.ui.controller("STTASOWD20.ext.controller.ListReportExtension");
			oGetFeatureStatusStub = oSandbox.stub(FeatureToggleService, "getFeatureStatus");
		});

		hooks.afterEach(() => {
			oSandbox.restore();
		});

		QUnit.test("should do things for active feature toggle", (assert) => {
			oGetFeatureStatusStub.withArgs("dummyFeatureActive").returns(true);
			assert.equal(oController.useAToggle(), "Toggle is active!");
		});

		QUnit.test("should do things for inactive feature toggle", (assert) => {
			oGetFeatureStatusStub.withArgs("dummyFeatureActive").returns(false);
			assert.equal(oController.useAToggle(), "Toggle is inactive!");
		});
	});
});

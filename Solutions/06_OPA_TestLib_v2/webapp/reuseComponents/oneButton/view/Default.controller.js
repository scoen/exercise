sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
	"use strict";
	Controller.extend("STTASOWD20.reuseComponents.oneButton.view.Default", {

		pressed: function (oEvent) {
			const oBindingContext = oEvent.getSource().getBindingContext();
			const oScheduleLine = oBindingContext.getObject();
			const oComponentModel = oEvent.getSource().getModel("componentModel");
			const oExtensionAPI = oComponentModel.getProperty("/extensionApi");
			const oNavigationController = oExtensionAPI.getNavigationController();
			const oNavigationData = {
				routeName: "toLevel4"
			};
			oNavigationController.navigateInternal(oScheduleLine.quantity, oNavigationData);
		}
	});
});

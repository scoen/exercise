sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/model/resource/ResourceModel",
	"sap/suite/ui/generic/template/extensionAPI/ReuseComponentSupport"
], function (UIComponent, ResourceModel, ReuseComponentSupport) {
	"use strict";

	return UIComponent.extend("STTASOWD20.reuseComponents.oneButton.Component", {
		metadata: {
			manifest: "json",
			properties: {
			}
		},


		init: function () {
			(UIComponent.prototype.init || jQuery.noop).apply(this, arguments);
			ReuseComponentSupport.mixInto(this, "componentModel");
		},

		stStart: function (oModel, oBindingContext, oExtensionAPI) {
			const oComponentModel = this.getComponentModel();
			oComponentModel.setProperty("/extensionApi", oExtensionAPI);
		}
	});
});

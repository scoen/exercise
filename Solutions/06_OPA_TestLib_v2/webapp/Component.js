sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
    "use strict";
    return AppComponent.extend("STTASOWD20.Component", {
        metadata: {
            "manifest": "json"
        }
    });
});

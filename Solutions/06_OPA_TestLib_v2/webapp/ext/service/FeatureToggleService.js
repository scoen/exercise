sap.ui.define([], () => {
    "use strict";

    let oInactiveFeatures = {};

    return {
        loadFeatureToggles(oModel) {
            oModel.read("/ToggleStatusSet", {
                success: function (oResponse) {
                    oInactiveFeatures = oModel.getObject("/");
                }
            });
        },

        getFeatureStatus(sId) {
            // if a feature toggle is returned by the feature maintenance API, it is inactive
            return !oInactiveFeatures.hasOwnProperty(`ToggleStatusSet('${sId}')`);
        }
    };
});

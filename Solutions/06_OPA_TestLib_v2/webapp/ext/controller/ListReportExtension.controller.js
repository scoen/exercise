sap.ui.define([
	"sap/base/Log",
	"sap/ui/core/Fragment",
	"sap/ui/core/message/Message",
	"sap/ui/core/MessageType",
	"STTASOWD20/ext/service/FeatureToggleService"
],
function (Log, Fragment, Message, MessageType, FeatureToggleService) {
	"use strict";

	sap.ui.controller("STTASOWD20.ext.controller.ListReportExtension", {

		onInit: function () {
			const oFeatureToggleModel = this.getOwnerComponent().getModel("featureToggle");
			FeatureToggleService.loadFeatureToggles(oFeatureToggleModel);
		},

		customizeMsgModelforTransientMessagesExtension: function () {
			const aInformationMessage = [];
			if (sap.ui.getCore().getMessageManager().getMessageModel().oData.length) {
				return new Promise(function (resolve, reject) {
					for (const i in sap.ui.getCore().getMessageManager().getMessageModel().oData) {
						const msg = sap.ui.getCore().getMessageManager().getMessageModel().oData[i];
						if (msg.persistent && msg.type === "Information") {
							aInformationMessage.push(msg);
						}
					}
					sap.ui.getCore().getMessageManager().removeMessages(aInformationMessage);
					const msgText = aInformationMessage.length ? "All Information message have been consolidated" : "There was no information Message from the backend";
					const consolidatedMessage = new sap.ui.core.message.Message({
						message: msgText,
						type: sap.ui.core.MessageType.Information,
						target: "",
						persistent: true
					});
					sap.ui.getCore().getMessageManager().addMessages(consolidatedMessage);
					resolve("customizeMsgModelforTransientMessagesExtension is completed");
				});
			}
		},

		onSaveAsTileExtension: function (oShareInfo) {
			oShareInfo.serviceUrl = ""; // Force a static tile on the FLP
		},

		beforeDeleteExtension: function (oBeforeDeleteProperties) {
			const oMessageText = {
					title: "My Title (Breakout with extensionAPI)",
					shortText: "My short text",
					unsavedChanges: "My unsaved changes",
					undeletableText: "My undeletable text"
			};

			return this.extensionAPI.securedExecution(function (){
				return new Promise(function (resolve){
					setTimeout(function (){
						resolve(oMessageText);
					}, 2000);
				});
			});
		},

		onItemsSelected: function (){
			const oApi = this.extensionAPI;
			const oNavigationController = oApi.getNavigationController();
			const aItemContexts = oApi.getSelectedContexts();
			let sKeys = "";
			let sSeparator = "";
			for (let i = 0; i < aItemContexts.length; i++){
				sKeys = sKeys + sSeparator + aItemContexts[i].getObject().so_id;
				sSeparator = ",";
			}
			oNavigationController.navigateInternal(sKeys || "None", {
				routeName: "SalesOrderListInfo"
			});
		},

		onCustomAction: function (){
			const oView = this.getView();

			const sFragmentName = FeatureToggleService.getFeatureStatus("NewFragmentName") ? "STTASOWD20.ext.fragments.NewDetails" : "STTASOWD20.ext.fragments.Details";

			if (!this.oDetailsDialog) {
				Fragment.load({id: oView.getId(), name: sFragmentName, controller: this})
				.then(function (oDetailsDialog) {
					this.oDetailsDialog = oDetailsDialog;
								oView.addDependent(this.oDetailsDialog);
								if (sFragmentName === "STTASOWD20.ext.fragments.NewDetails") {
									this.oDetailsDialog.open();
								} else if (this._onBindDetails()) {
									this.oDetailsDialog.open();
								}
				}.bind(this))
				.catch(function (exception) {
					Log.error(exception);
				});
			} else {
				this.oDetailsDialog.open();
			}
		},

		_onBindDetails: function () {
			const oResponsiveTable = this.byId("responsiveTable");
			const oSelectecdItems = oResponsiveTable.getSelectedItems();
			if (oSelectecdItems.length < 2) {
				this.byId("smartFormDetails").bindElement(oSelectecdItems[0].getBindingContext().getPath());
				return true;
			}
				this._errorDetailMessage();
				return false;

		},

		_errorDetailMessage: function () {
			const oMessage = new Message({
				message: "test",
				type: MessageType.Error,
				target: "/C_STTA_SalesOrder_WD_20",
				processor: this.getView().getModel()
			});
			sap.ui.getCore().getMessageManager().addMessages(oMessage);
		},

		onCloseDetailDialog: function () {
			this.oDetailsDialog.close();
		},

		useAToggle: function () {
			if (FeatureToggleService.getFeatureStatus("dummyFeatureActive")) {
				return "Toggle is active!";
			}
			return "Toggle is inactive!";
		}
	});
});

sap.ui.controller("STTASOWD20.implementingComponents.productCanvas.view.Default", {

	onInit: function () {
		const oComponent = this.getOwnerComponent();
		const oComponentModel = oComponent.getComponentModel();
		oComponentModel.setProperty("/View", this.getView());
	},

	onPress: function () {
		const oComponent = this.getOwnerComponent();
		const oComponentModel = oComponent.getComponentModel();
		const oNavigationController = oComponentModel.getProperty("/navigationController");
		oNavigationController.navigateInternal();
	},

	onClick: function () {
		const oComponent = this.getOwnerComponent();
		const oComponentModel = oComponent.getComponentModel();
		const oExtensionAPI = oComponentModel.getProperty("/oExtensionAPI");
		oExtensionAPI.refreshAncestors();
	}
});

sap.ui.controller("STTASOWD20.implementingComponents.salesOrderItemInfo.view.Default", {

	onInit: function () {
		const oComponent = this.getOwnerComponent();
		const oComponentModel = oComponent.getComponentModel();
		oComponentModel.setProperty("/View", this.getView());
	}
});

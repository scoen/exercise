module.exports = function (config) {
  "use strict";
  config.set({
    frameworks: ["ui5"],
    ui5: {
      url: "https://sapui5.hana.ondemand.com"
    },

    browsers: ["ChromeCustom"],
    customLaunchers: {
      ChromeCustom: {
        base: "Chrome",
        flags: ["--start-maximized"]
      },
      ChromeCustomHeadless: {
        base: "ChromeHeadless",
        flags: ["--window-size=1600,900"]
      }
    },

    browserConsoleLogOptions: {
      level: "warn"
    },

    preprocessors: {
      "**/webapp/!(test|localService)/**/*.js": ["coverage"]
    },
    coverageReporter: {
      includeAllSources: true,
      reporters: [
        {
          type: "html",
          dir: "./target/coverage"
        },
        {
          type: "text"
        }
      ]
    },

    htmlReporter: {
      outputFile: "./target/html/QUnit.html",

      // Optional
      pageTitle: "Test Results",
      subPageTitle: "Detailed test results",
      groupSuites: true,
      useCompactStyle: true,
      useLegacyStyle: true,
      showOnlyFailed: false
    },
    reporters: ["progress", "coverage", "html"]
  });
};

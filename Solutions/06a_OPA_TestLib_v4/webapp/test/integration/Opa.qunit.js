sap.ui.require(
    [
        "sap/fe/test/JourneyRunner",
        "sap/fe/test/Shell",
        "sap/fe/demo/incidents/test/integration/pages/MainListReport",
        "sap/fe/demo/incidents/test/integration/pages/MainObjectPage",
        "sap/fe/demo/incidents/test/integration/OpaJourney",
        "sap/fe/demo/incidents/test/integration/ErrorJourney"
    ],
    function(JourneyRunner, Shell, MainListReport, MainObjectPage, Journey, ErrorJourney) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('sap/fe/demo/incidents') + '/index.html',
            opaConfig: { timeout: 40 }
        });
        
        JourneyRunner.run(
            {
                pages: { onTheMainPage: MainListReport, onTheDetailPage: MainObjectPage }
            },
            Journey.run,
            ErrorJourney.run
        );
    }
);
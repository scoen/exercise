sap.ui.define(['sap/ui/test/opaQunit'], function (opaTest) {
  'use strict';

  var Journey = {
    run: function () {
      QUnit.module('Sample journey');
      let incidentTitle = "Password Reset";

      opaTest("SAPUIVersion: " + sap.ui.version, function (Given, When, Then) {
        sap.ui.test.Opa5.assert.ok(true, sap.ui.version);
      });

      opaTest('#0: Start', function (Given, When, Then) {
        Given.iResetTestData().and.iStartMyApp("incidents-tile");
        Then.onTheMainPage.iSeeThisPage();
      });

      opaTest('#1: ListReport: Check List Report Page loads', function (Given, When, Then) {
        When.onTheMainPage.onFilterBar().iExecuteSearch();
        Then.onTheMainPage.onTable().iCheckRows();
      });

      opaTest('#2: ListReport: Expect 2 entries when searching for "Password"', function (Given, When, Then) {
        When.onTheMainPage.onFilterBar().iChangeSearchField("Password").and.iExecuteSearch();
        Then.onTheMainPage.onTable().iCheckRows(2);
      });

      opaTest('#3: ListReport: Navigate to Object Page', function (Given, When, Then) {
        When.onTheMainPage.onTable().iPressRow({title: incidentTitle});
        Then.onTheDetailPage.iSeeThisPage();
        Then.onTheDetailPage.onHeader().iCheckTitle(incidentTitle);
      });

      opaTest('#4: ObjectPage: Navigate back to List Report', function (Given, When, Then) {
        When.onTheShell.iNavigateBack();
        Then.onTheMainPage.iSeeThisPage();
      });

      opaTest('#5: ListReport: Check custom action state when selecting a row', function (Given, When, Then) {
        When.onTheMainPage.onTable().iSelectRows({title: incidentTitle});
        Then.onTheMainPage.onTable().iCheckAction("Custom Action", { enabled: true });
      });

      opaTest('#6: ListReport: Check first action handler', function (Given, When, Then) {
        When.onTheMainPage.onTable().iExecuteAction("Custom Action");
        Then.onTheMainPage.onConfirmationDialog().iCheckState({ title: "Call Result" });
        Then.onTheMainPage.iSeeFirstActionHandler();
      });

      opaTest('#7: ListReport: Close messagebox', function (Given, When, Then) {
        When.onTheMainPage.onDialog().iConfirm();
        Then.onTheMainPage.iSeeThisPage();
      });

      opaTest('#999: Tear down', function (Given, When, Then) {
        Given.iTearDownMyApp();
      });
    }
  };

  return Journey;
});
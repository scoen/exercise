sap.ui.define(['sap/fe/test/ListReport', 'sap/ui/test/OpaBuilder', 'sap/ui/test/Opa5'], function (ListReport, OpaBuilder, Opa5) {
    'use strict';

    var AdditionalCustomListReportDefinition = {
        actions: {},
        assertions: {
            iSeeFirstActionHandler: function () {
                return OpaBuilder.create(this)
                    .hasType("sap.m.Text")
                    .isDialogElement()
                    .hasProperties({ text: "Our first Action Handler" })
                    .description("Has expected text")
                    .execute();            
            }
        }
    };

    return new ListReport(
        {
            appId: 'sap.fe.demo.incidents',
            componentId: 'IncidentsList',
            entitySet: 'Incidents'
        },
        AdditionalCustomListReportDefinition
    );
});
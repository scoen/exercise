module.exports = {
    executeAction: function (actionDefinition, actionData, keys) {
        console.log(actionDefinition.name);
        if (actionDefinition.name === "customAction") {
            if (keys.ID === "2feec9ec-c192-48c6-8971-e3c4838cb861") {
                this.throwError('Testing Error', 400, { error: { message: 'Our Error Action Handler', code: '123' } });
            }
            else if (keys.ID === "919f1a94-0281-4226-ad3a-9148af4cb5d2") {
                return { TravelUUID: "00558871-34eb-fb88-1700-2df065f8b6af", IsActiveEntity: false, Description: "Our first Action Handler" };;
            } else {
                return { xDescription: "Our Second Action Handler" };;
            }
        }
    }
};
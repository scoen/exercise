sap.ui.define(
	["sap/ui/core/Fragment", "sap/m/MessageBox"],
	function (Fragment, MessageBox) {
		"use strict";
		return {
			enabledForSelectedOnly: function (oBindingContext, aSelectedContexts) {
				if (aSelectedContexts && aSelectedContexts.length === 1) {
					return true;
				}
				return false;
			},

			onCustomAction: function (oBindingContext, aSelectedContexts) {
                var oView = this._view;
                var oModel = oView.getModel();
                var oAction = oModel.bindContext(aSelectedContexts[0].getPath() + "/IncidentService.customAction(...)", aSelectedContexts[0]);
               
                oAction.execute().then(
                    function () {
                        var oResult = oAction.getBoundContext().getObject();
                        var sResultString = oResult.Description;
						var icon = MessageBox.Icon.INFO;
                        if (!sResultString) {
                            sResultString = oResult.xDescription;
                            icon = MessageBox.Icon.QUESTION;
                        }
                        MessageBox.alert(sResultString, {
                            icon: MessageBox.Icon.INFO,
                            title: "Call Result"
                        });
                    },
                    function (oError) {
                    	MessageBox.alert(oError.message, {
                            icon: MessageBox.Icon.ERROR,
                            title: "Error"
                        });
                    }
                );
            }
		};
	}
);
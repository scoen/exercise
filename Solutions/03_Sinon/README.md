# Solution - Unit-Tests with QUnit and Sinon

## Instructions

Write QUnit tests for the app Shopping Cart to convert a number and return it as price in a specific currency. Use sinon to isolate your tests from a Currency Converter that is not implemented.

1. Go to the **Exercises/03_SINON** folder.
2. Create QUnit tests for formatter.convertNumberIntoCurrency and isolate your tests from the Currency Converter.
3. Write tests that cover the following aspects:
   - How to use the Sandbox:

   ```javascript
   var sandbox;
   QUnit.module("formatter - convertNumberIntoCurrency", {
   	beforeEach: function () {
   		sandbox = sinon.sandbox.create();
   	},
   	afterEach: function () {
   		sandbox.restore();
   	}
   });
   ```

   - Use a stub to check that the Currency Converter is **called with the right parameter** and is **called once**?

   ```javascript
   QUnit.test("Should call formatter.price once with correct parameters", function (assert) {
     var spy = sandbox.spy(formatter, "price");
     formatter.convertNumberIntoCurrency(123, "INR");
     assert.ok(spy.calledWith(123), "formatter.price was called with passed number");
     assert.ok(spy.calledOnce, "formatter.price was called once");
   });
   ```

   - Use a **stub** to check that the Currency Converter function `getPriceInCurrency` is **called with the right parameter** and is **called once** in our function under test `convertNumberIntoCurrency`?

    ```javascript
    QUnit.test("Should call Currency Converter once with correct parameters", function (assert) {
      var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
      formatter.convertNumberIntoCurrency(123, "INR");
      assert.ok(stub.calledWith("123.00", "INR"), "Converter was called with correct price");
      assert.ok(stub.calledOnce, "Converter was called once");
    });
    ```

    - Use a stub to check that the Currency Converter **called not at all** if you pass NaN or an undefined currency to it?

    ```javascript
    QUnit.test("Should not call Currency Converter with NaN", function (assert) {
      var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
      formatter.convertNumberIntoCurrency("Test", "USD");
      assert.ok(stub.notCalled, "Converter was not called");
    });

    QUnit.test("Should not call Currency Converter w/o currency", function (assert) {
      var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
      formatter.convertNumberIntoCurrency(123);
      assert.ok(stub.notCalled, "Converter was not called");
    });
    ```

   - Verify that formatter.convertNumberIntoCurrency returns the converted price with currency by **stubbing the return value** of the Currency Converter, e.g. "9000 INR"

    ```javascript
    QUnit.test("Should return converted price with currency", function (assert) {
      var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency").returns("9600.00");
      var priceInIndianRupees = formatter.convertNumberIntoCurrency(123, "INR");
      assert.equal(priceInIndianRupees, "9600.00 INR", "Concatenated price is returned")
    });
    ```

   - Verify that the formatter.convertNumberIntoCurrency returns the converted price with currency by **returning different stub values** for the first and the second call of the Currency Converter

    ```javascript
      QUnit.test("Should return different converted prices", function (assert) {
        var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
        stub.onCall(0).returns("9600.00");
        stub.onCall(1).returns("9700.00");
        var priceInIndianRupeesFirstCall = formatter.convertNumberIntoCurrency(123, "INR");
        var priceInIndianRupeesSecondCall = formatter.convertNumberIntoCurrency(123, "INR");
        assert.equal(priceInIndianRupeesFirstCall, "9600.00 INR", "Concatenated price is returned");
        assert.equal(priceInIndianRupeesSecondCall, "9700.00 INR", "Concatenated price is returned");
        assert.ok(stub.calledTwice);
    });
    ```

4. Run the tests:
    1. Open a terminal in the exercise root folder.
    2. Run `npm run start`
    3. Open the Unit Test Suite.
    4. Alternatively use karma testrunner `npm run test:watch` in exercise root folder.

## Optional exercises

1. Add a test that verifies that the function `formatter.waitForCurrencyConversion` saves the converted currency to a variable in the `formatter` object.  
Use a mock to specify the behavior of the function `CurrencyConverter.getPriceInCurrency` and [Sinon Fake Timers](https://sinonjs.org/releases/v1.17.6/fake-timers/) to simulate the timeout.

```javascript
QUnit.test("Should wait for 500 ms", (assert) => {
			var clock = sinon.useFakeTimers();

			var currencyConverter = sandbox.mock(CurrencyConverter);
			currencyConverter.expects("getPriceInCurrency").withArgs("200.00", "EUR").returns("150");
			
			const waitingTimeMillis = 500;
			formatter.waitForCurrencyConversion(200, "EUR", waitingTimeMillis);

			assert.strictEqual(formatter.convertedPrice, "", "Converted Price is empty before the waiting time")
			clock.tick(waitingTimeMillis);
			assert.strictEqual(formatter.convertedPrice, "150 EUR", "Converted Price is saved after waiting time");
}); 
```

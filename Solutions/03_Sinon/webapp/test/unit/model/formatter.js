sap.ui.define([
	'sap/ui/demo/cart/model/formatter',
	'sap/ui/demo/cart/model/CurrencyConverter',
	'sap/ui/thirdparty/sinon-4'
], function (formatter, CurrencyConverter, sinon) {
	"use strict";

	var sandbox;

	var sDefaultLanguage = sap.ui.getCore().getConfiguration().getLanguage();

	QUnit.module("formatter - statusText", () => {

		QUnit.test("Should see statusText 'Available'", function (assert) {
			assert.equal(formatter.statusText('A'), "Available", "Status Text is correct");
		});
	
		QUnit.test("Should see statusText 'Out of Stock'", function (assert) {
			assert.equal(formatter.statusText('O'), "Out of Stock", "Status Text is correct");
		});
	
		QUnit.test("Should see statusText 'Discontinued'", function (assert) {
			assert.equal(formatter.statusText('D'), "Discontinued", "Status Text is correct");
		});
	
		QUnit.test("Should see statusText 'Other'", function (assert) {
			assert.equal(formatter.statusText('Other'), "Other", "Status Text is correct");
		});
	});


	QUnit.module("formatter - price", (hooks) => {

		hooks.before(function () {
			sap.ui.getCore().getConfiguration().setLanguage("en-US");
		});

		hooks.after(function () {
			sap.ui.getCore().getConfiguration().setLanguage(sDefaultLanguage);
		});

		QUnit.test("Should have a price function", function (assert) {
			assert.ok(formatter.hasOwnProperty("price"), "There is a price function");
		});

		QUnit.test("Price should return error when parameter is not number", function (assert) {
			assert.equal(formatter.price("Text"), "Parameter is not a number", "Error is returned");
		});

		QUnit.test("Price should return number with two digits after comma", function (assert) {
			assert.equal(formatter.price(123), "123.00", "Price is formatted with two digits");
		});

		QUnit.test("Price should return number . as seperator for big numbers", function (assert) {
			assert.equal(formatter.price(12345), "12,345.00", "Price is formatted with two digits and dot");
		});

	});

	QUnit.module("formatter - Currency convertor", (hooks) => {

		hooks.before(function () {
			sandbox = sinon.sandbox.create();
		});

		hooks.afterEach(function () {
			sandbox.restore();
		});

		QUnit.test("Should call formatter.price once with correct parameters", function (assert) {
			var spy = sandbox.spy(formatter, "price");
			formatter.convertNumberIntoCurrency(123, "INR");
			assert.ok(spy.calledWith(123), "formatter.price was called with passed number");
			assert.ok(spy.calledOnce, "formatter.price was called once");
		});

		QUnit.test("Should call Currency Converter once with correct parameters", function (assert) {
			var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
			formatter.convertNumberIntoCurrency(123, "INR");
			assert.ok(stub.calledWith("123.00", "INR"), "Converter was called with correct price");
			assert.ok(stub.calledOnce, "Converter was called once");
		});

		QUnit.test("Should not call Currency Converter with NaN", function (assert) {
			var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
			formatter.convertNumberIntoCurrency("Test", "USD");
			assert.ok(stub.notCalled, "Converter was not called");
		});

		QUnit.test("Should not call Currency Converter w/o currency", function (assert) {
			var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
			formatter.convertNumberIntoCurrency(123);
			assert.ok(stub.notCalled, "Converter was not called");
		});


		QUnit.test("Should return converted price with currency", function (assert) {
			var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency").returns("9600.00");
			var priceInIndianRupees = formatter.convertNumberIntoCurrency(123, "INR");
			assert.equal(priceInIndianRupees, "9600.00 INR", "Concatenated price is returned");
		});

		QUnit.test("Should return different converted prices", function (assert) {
			var stub = sandbox.stub(CurrencyConverter, "getPriceInCurrency");
			stub.onCall(0).returns("9600.00");
			stub.onCall(1).returns("9700.00");
			var priceInIndianRupeesFirstCall = formatter.convertNumberIntoCurrency(123, "INR");
			var priceInIndianRupeesSecondCall = formatter.convertNumberIntoCurrency(123, "INR");
			assert.equal(priceInIndianRupeesFirstCall, "9600.00 INR", "Concatenated price is returned");
			assert.equal(priceInIndianRupeesSecondCall, "9700.00 INR", "Concatenated price is returned");
			assert.ok(stub.calledTwice);
		});
	});

	QUnit.module("Wait for currency conversion", (hooks) => {

		hooks.before(function () {
			sandbox = sinon.sandbox.create();
		});

		hooks.afterEach(function () {
			sandbox.restore();
		});

		QUnit.test("Should wait for 500 ms", (assert) => {
			var clock = sinon.useFakeTimers();

			var currencyConverter = sandbox.mock(CurrencyConverter);
			currencyConverter.expects("getPriceInCurrency").once().withExactArgs("200.00", "EUR").returns("150");
			
			const waitingTimeMillis = 500;
			formatter.waitForCurrencyConversion(200, "EUR", waitingTimeMillis);

			assert.strictEqual(formatter.convertedPrice, "", "Converted Price is empty before the waiting time")
			clock.tick(waitingTimeMillis);
			assert.strictEqual(formatter.convertedPrice, "150 EUR", "Converted Price is saved after waiting time");

			// needed to verify that all expectations on the mock are met
			currencyConverter.verify();
		});

	});
});
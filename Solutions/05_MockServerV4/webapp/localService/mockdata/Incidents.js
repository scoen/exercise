module.exports = {
    executeAction: function (actionDefinition, actionData, keys) {
        console.log('Updating the data for ' + JSON.stringify(keys));
        if (actionDefinition.name === "myCustomAction") {
            const oRelevantEntry = this.base.getAllEntries().find((oEntry) => oEntry.ID === keys.ID)
            oRelevantEntry.title = "Custom Action: Title Changed"
            this.base.updateEntry(keys, oRelevantEntry);
        }
        
    }
};

# 07 - Refactoring

The goal of this exercise is not to finish all the described steps in the given time,
but to learn the automated refactoring features of your IDE and how you can make your code cleaner
with a series of small, automated steps.

Since the steps in the exercise readme are only one possibility to refactor the code, every developer can come to a different ending state.
Therefore, there is no solution available for this exercise.

However, there is a recording of a refactoring demo available that may help you in case you run into problems.  
Please contact your trainers if you are interested in the recording.

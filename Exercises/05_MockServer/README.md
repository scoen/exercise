# Exercise - Run your App with the Mock Server

**Duration:** 45 minutes

**Hint:** If you get into problems, check the [troubleshooting section](../04_OPA/README.md#troubleshooting---common-mistakes).

## Instructions

1. Create the **initMockServer.js** file in your webapp/test folder with the following content and replace the placeholders:

```
sap.ui.define([
	"<Reference to mockserver.js in your localService folder>",
	"sap/m/MessageBox"
], function (mockserver, MessageBox) {
	"use strict";

	// initialize the mock server
	mockserver.init().catch(function (oError) {
		MessageBox.error(oError.message);
	}).finally(function () {
		// initialize the embedded component on the HTML page
		sap.ui.require(["sap/ui/core/ComponentSupport"]);
	});
});
```

2. Create the **mockServer.html** page in your webapp/test folder with the following content and replace the placeholders:
```
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Shopping Cart</title>

	<script
		id='sap-ui-bootstrap'
		src='https://sapui5.hana.ondemand.com/resources/sap-ui-core.js'
		data-sap-ui-theme='sap_fiori_3'
		data-sap-ui-compatVersion="edge"
		data-sap-ui-async="true"
		data-sap-ui-oninit="module:<Reference to initMockServer>"
		data-sap-ui-resourceroots='{
			"<Namespace of your app>" : "../",
			"sap.ui.demo.mock": "https://sapui5.hana.ondemand.com/test-resources/sap/ui/documentation/sdk/"
		}'>
	</script>

</head>
<body class='sapUiBody' id='content'>
	<div data-sap-ui-component data-name="<Namespace of your app>" data-id="container" data-settings='{"id" : "cart"}'></div>
</body>
</html>
```
3. Fill in your app path in *localService/mockserver.js*: sap/ui/..
4. Extend mock data to appropriate JSON files for the app *Shopping Cart* by...
  - Adding a new product category, e.g. add "Training Accessories"
  - Adding several products to this product category, e.g. add a "USB Wireless Presenter"
5. Call and Mock a Function Import
  - Add a new button in the footer of the Product view (*webapp/view/Product.view.xml*). You can use the existing button to add a product to the cart as a template.
  - Assign the function **onReserveProduct** from the **BaseController** to the button.
  > The function **onReserveProduct** calls a function import that returns *success: true* if the product could be reserved. Otherwise it returns: *success: false*
  - Add a custom mock behaviour for function import **ReserveProduct**:
    - Fill in the missing parts in the **localService/mockserver.js** file: Look for commented code blocks, uncomment them and replace the placeholders.
      - Respond with the code for **status OK**, a suitable **content type** for a JSON object and a string representation of the result. You can find some templates in the sinonjs documentation.
    - The result should be either *success: true* if *productId=HT-2001* is passed to the function import (Accessories: 10" Portable DVD player)
    - ... or *success: false* if *productId=HT-2000* or *productId=HT-2026* is passed to the function import (Accessories: 7" Widescreen Portable DVD Player w MP3 or Accessories: Audio/Video Cable Kit - 4m)
6. Run the app with mockserver:
  - Open a terminal in the exercise root folder.
  - Open the project structure in your browser `ui5 serve -o`
  - Go into webapp/test subfolder and open mockserver.html
7. Verify that the new Product Category and Product are available.
8. Select the products in category *Accessories* with the mocked function import behavior and verify that the app behaves accordingly.

## Hints - QUnits
- Find a suitable Regex in mockserver.js to return *success: true* or *false* according to the product id, e.g. "ReserveProduct.?productId=HT-2001"
- Refer to **Fake XHR and server** in the sinon.js documentation for further details how to define a suitable response
You can use the following code snippet as template:
```
// custom mock behaviour may be added here
aRequests.push({
	method: "GET",
	path: new RegExp("<Enter RegExp here>"),
	response: function (oXhr) {
		Log.debug("Incoming request for ReserveProduct");
		oXhr.respond(<status-code>, {
			"Content-Type": "application/json;charset=utf-8"
		}, JSON.stringify({d: {
			<Expected content>
		}}));
		return true; // Skip default processing
	}
});
```

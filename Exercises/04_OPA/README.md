# OPA Exercise

**Duration:** 45 minutes

**Hint:** If you get into problems, check the [troubleshooting section](#troubleshooting---common-mistakes).

## Instructions

1. Go to the **Exercises/04_OPA** folder.
2. Navigate into the integration folder under test

3. Add the following snippet in your AllJourneys.js file:

```javascript
sap.ui.define([
	"sap/ui/test/Opa5",
	"./arrangements/Startup",
	"./NavigationJourney"
], function (Opa5, Startup) {
	"use strict";

	Opa5.extendConfig({
		arrangements: new Startup(),
		autoWait: true
	});
});
```

4. Create a new file named NavigationJourney.js 

```javascript
/* global QUnit */

sap.ui.define([
	"sap/ui/test/opaQunit",
], function (opaTest) {
	"use strict";

	QUnit.module("Navigation Journey");
	
	opaTest("Description of the test case", function (Given, When, Then) {
		// Arrangements
		
		// Actions
		
		// Assertions
		
	});

});
```

5. Now add your first test following the given-when-then style

- Start the application,
- Select on a Product Category
- Select a Product
- Check that the Product is displayed

6. Create a pages folder and then create a new page object inside the folder named after the view you want to write tests for (e.g. home.js)

```javascript
sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/matchers/Properties"
], function (
	Opa5,
	Press,
	Properties) {
	"use strict";

	Opa5.createPageObjects({
		onTheHomePage: {
			actions: {
				
			},

			assertions: {
				
			}
		}
	});
});
```

7. Use the UI5 Test Recorder to identify suitable actions and assertions
   - Press CTRL+ALT+SHIFT+T (SHIFT-CTRL-OPTION-T for Mac) to open the Test Recorder
   - Right click on UI element to generate control locator
   - Copy Snippet and use it in page object

8.  Implement all actions and assertions used in your NavigationJourney.js

9.  Run the tests:
    1. Open a terminal in the exercise root folder.
    2. Run `npm run start`
    3. Open the OPA Test Suite.
    4. Alternatively use karma testrunner `npm run test:watch` in exercise root folder.

10. Add more tests to the journey file to build a real navigation journey (Tipp: You may need to implement more page objects)

## Hints - OPA

- In your journeys you need a reference to your page objects. This is done in the define part:

```javascript
sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/<Name of your page object file>",
```

- Always try to follow the Give-When-Then pattern. Given is typically only needed for the first step in a Journey.

```javascript
Given.iStartMyApp();
When.onTheHomePage.iClickOnButton();
Then.onTheHomePage.iShouldSeeAPopup();
```

- **Actions**: You can copy actions directly from the Test Recorder
- **Assertions**: You need at least one assert statement in a **success** method

```javascript
success: function(oObject / aObjects) {
	Opa5.assert.ok(..., "Found object with ID...");
	Opa5.assert.strictEqual(..., "Found object with binding Path HT-...");
	...
},
```

- To enhance the readability of your tests you should add an **errorMessage** that is displayed if no element(s) could be found

```javascript
errorMessage: "The object with ID ... could not be found"
```

- If the identification of an element is not working with an ID that contains generated parts than you use a regex.
With $ at the end of the regex you search for controls which end on the given regex.

```javascript
id: /finalPartofID$/
```

## Troubleshooting - Common Mistakes

* Spelling mistakes because of case sensitivity: E.g. `U` in `QUnit`, `S` in `localService`
* Other spelling mistakes, e.g. `fomatter` instead of `formatter`
* Forgotten trailing slashes or dots, e.g. in `viewNamespace: "sap.ui.demo.cart.view."`, `_sAppPath = "sap/ui/demo/cart/"`
* Duplicated viewName definition, e.g. in `AllJourneys.js` -> `Opa5.extendConfig` and PageObjects -> viewNames get concatenated. <br>
Be careful: Snippets copied from TestRecorder contain viewName, this has to be deleted if already defined!
* Duplicated PageObject names because of copy-paste
* `.` instead of `/` or the other way round: `.`  for names, ids, resourceroot keys, `/` for references
* Missing imports, e.g. `Press`
* PageObject names diverging from names used in Journeys
* If you face an issue with missing plugins during the startup of karma enter `npm link <name of the plugin>` in the terminal, e.g. `npm link karma-webdriver-launcher`.

sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/model/resource/ResourceModel",
	"sap/suite/ui/generic/template/extensionAPI/ReuseComponentSupport"
], function (UIComponent, ResourceModel, ReuseComponentSupport) {
	"use strict";

	return UIComponent.extend("STTASOWD20.reuseComponents.tableTest.Component", {
		metadata: {
			manifest: "json",
			properties: {
				stIsAreaVisible: {
					 type: "boolean",
					 group: "standard"
				},
				entitySet: {
						type: "string",
						group: "specific",
						defaultValue: ""
				},
				navigationProperty: {
					type: "string",
					group: "specific",
					defaultValue: ""
				}
			}
		},

		setEntitySet: function (sKey) {
			const oComponentModel = this.getComponentModel();
			oComponentModel.setProperty("/entitySet", sKey);
		},

		setNavigationProperty: function (sKey) {
			const oComponentModel = this.getComponentModel();
			oComponentModel.setProperty("/navigationProperty", sKey);
		},

		setStIsAreaVisible: function (bIsAreaVisible) {
			if (bIsAreaVisible !== this.getStIsAreaVisible()){
				this.setProperty("stIsAreaVisible", bIsAreaVisible);
				if (bIsAreaVisible) {
					const oComponentModel = this.getComponentModel();
					const sBindingPath = oComponentModel.getProperty("/entitySet");
					const oController = this.getRootControl().getController();
					const oTable = this.byId("STTASOWD20::sap.suite.ui.generic.template.ObjectPage.view.Details::C_STTA_SalesOrder_WD_20--STTASOWD20.reuseComponents.tableTest::tableTest::ComponentContainerContent---View--idProductsTable");
					const oListItem = new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({text: "{ProductID}"}),
							new sap.m.Text({text: "{Category}"}),
							new sap.m.Text({text: "{CurrencyCode}"}),
							new sap.m.Text({text: "{Price}"})
						]
					});

					oTable.bindItems({
						path: sBindingPath,
						template: oListItem
					});

					oListItem.setType("Navigation");
					oListItem.attachPress(oController.onItemPress);
				}
			}
		},

		init: function () {
			(UIComponent.prototype.init || jQuery.noop).apply(this, arguments);
			ReuseComponentSupport.mixInto(this, "componentModel");
		},

		stStart: function (oModel, oBindingContext, oExtensionAPI) {
			const oComponentModel = this.getComponentModel();
			oComponentModel.setProperty("/extensionApi", oExtensionAPI);
		},

		stRefresh: function (oModel, oBindingContext, oExtensionAPI) {

		}
	});
});

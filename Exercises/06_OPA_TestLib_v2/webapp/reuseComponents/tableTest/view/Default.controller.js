sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
	"use strict";
	Controller.extend("STTASOWD20.reuseComponents.tableTest.view.Default", {

		onItemPress: function (oEvent) {
			const oBindingContext = oEvent.getSource().getBindingContext();
			const oProduct = oBindingContext.getObject();
			const oComponentModel = oEvent.getSource().getModel("componentModel");
			const oExtensionAPI = oComponentModel.getProperty("/extensionApi");
			const oNavigationController = oExtensionAPI.getNavigationController();
			const oNavigationData = {
				routeName: "to_Product"
			};
			oNavigationController.navigateInternal(oProduct.ProductID, oNavigationData);
		}
	});
});

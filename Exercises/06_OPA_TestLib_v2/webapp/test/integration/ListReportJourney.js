sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/ListReport"
], function (opaTest) {
		"use strict";
		QUnit.module("ListReport Journey");

		opaTest("Starting the app and search", function (Given, When, Then) {
			Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display"});
			When.onTheListReportPage.iClickTheGoButton();
			Then.onTheListReportPage.theSmartTableIsFilled();
			Then.iLeaveMyFLPApp();
		});
	}
);

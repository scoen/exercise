sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"sap/ui/test/matchers/AggregationFilled",
	"sap/ui/test/actions/Press",
	"sap/ui/model/resource/ResourceModel"
], function (Opa5, PropertyStrictEquals, AggregationFilled, Press, ResourceModel) {
	"use strict";

	Opa5.createPageObjects({
		onTheListReportPage: {
			actions: {
				iClickTheGoButton: function () {
					return this.waitFor({
						id: "STTASOWD20::sap.suite.ui.generic.template.ListReport.view.ListReport::C_STTA_SalesOrder_WD_20--listReportFilter-btnGo",
						actions: new Press({
							idSuffix: "BDI-content"
						})
					});
				}
			},

			assertions: {
				theSmartTableIsFilled: function () {
					return this.waitFor({
						controlType: "sap.ui.comp.smarttable.SmartTable",
						aggregationFilled: {
							name: "items"
						},
						success: function (aTable) {
							Opa5.assert.ok(true, "The Smart Table is filled correctly on the List Report");
						},
						errorMessage: "The Smart Table on the List Report is not filled"
					});
				}
			}
		}
	});
});

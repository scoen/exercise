sap.ui.define([
	"sap/ui/test/Opa5",
	"STTASOWD20/localService/C_STTA_SalesOrder_WD_20/mockserver",
	"STTASOWD20/localService/featureToggleService/mockserver",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/core/routing/HashChanger",
	"STTASOWD20/test/flpSandbox",
	"sap/ui/fl/FakeLrepConnectorLocalStorage"
], function (Opa5, defaultMockserver, featureToggleMockserver, ODataModel, HashChanger, flpSandbox, FakeLrepConnectorLocalStorage) {
	"use strict";

	return Opa5.extend("STTASOWD20.test.integration.arrangements.component.Startup", {
		/**
		 * Initializes mock server, then start the app component
		 * @param {object} oOptionsParameter An object that contains the configuration for starting up the app.
		 * @param {integer} oOptionsParameter.delay A custom delay to start the app with
		 * @param {integer} oOptionsParameter.keepStorage Does not clear the local storage when set to true
		 * @param {string} [oOptionsParameter.hash] The in app hash can also be passed separately for better readability in tests
		 * @param {boolean} [oOptionsParameter.autoWait=true] Automatically wait for pending requests while the application is starting up.
		 */
		iStartMyFLPApp: function (oOptionsParameter) {
			const oOptions = oOptionsParameter || {};

			this._clearSharedData();

			// start the app with a minimal delay to make tests fast but still async to discover basic timing issues
			oOptions.delay = oOptions.delay || 1;

			const aInitializations = [];

			// configure mock server with the current options
			aInitializations.push(defaultMockserver.init(oOptions));
			aInitializations.push(flpSandbox.init());

			// Wait for all initialization promises of mock server and sandbox to be fulfilled.
			// After that enable the fake LRepConnector
			this.iWaitForPromise(Promise.all(aInitializations));
			FakeLrepConnectorLocalStorage.enableFakeConnector();

			this.waitFor({
				autoWait: (oOptions ? oOptions.autoWait : true),
				success: function () {
					new HashChanger().setHash(oOptions.intent + (oOptions.hash ? "&/" + oOptions.hash : ""));
				}
			});
		},


		_clearSharedData: function () {
			// clear shared metadata in ODataModel to allow tests for loading the metadata
			ODataModel.mSharedData = { server: {}, service: {}, meta: {} };
		}
	});
});

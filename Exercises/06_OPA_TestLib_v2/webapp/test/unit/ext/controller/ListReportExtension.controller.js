sap.ui.define([
	"STTASOWD20/ext/service/FeatureToggleService",
	"sap/ui/thirdparty/sinon-4"
], function (FeatureToggleService, sinon) {
	"use strict";

	QUnit.module("Evaluate feature toggle", function (hooks) {

		let oSandbox = null;
		let oGetFeatureStatusStub = null;
		let oController = null;

		hooks.before(() => {
			oSandbox = sinon.sandbox.create();
		});

		hooks.beforeEach(() => {
			oController = sap.ui.controller("STTASOWD20.ext.controller.ListReportExtension");
			oGetFeatureStatusStub = oSandbox.stub(FeatureToggleService, "getFeatureStatus");
		});

		hooks.afterEach(() => {
			oSandbox.restore();
		});

		QUnit.test("should do things for active feature toggle", (assert) => {
			// add a test
			// Example for stubbing the FeatureToggleService (change according to your needs):
			oGetFeatureStatusStub.withArgs("dummyFeatureActive").returns(true);
			assert.equal(true, 1);
		});

		QUnit.test("should do things for inactive feature toggle", (assert) => {
			// add a test
			assert.equal(false, 0);

		});
	});
});

sap.ui.define(["sap/ui/core/UIComponent",
				"sap/suite/ui/generic/template/extensionAPI/ReuseComponentSupport"],
function (UIComponent, ReuseComponentSupport) {
 "use strict";

	/* Definition of the reuse component */
	return UIComponent.extend("STTASOWD20.implementingComponents.salesOrderItemInfo.Component", {
		metadata: {
			manifest: "json",
			library: "STTASOWD20.implementingComponents.salesOrderInfo",
			properties: {
				/* Standard properties for reuse components */

				/* UI mode accross fiori applications so the component knows in what mode the application is running
				 * Defined in sap/suite/ui/generic/template/extensionAPI/UIMode
				 */
				uiMode: {
					type: "string",
					group: "standard"
				}
			}
		},

		// Standard life time event of a component. Used to transform this component into a reuse component for smart templates and do some initialization
		init: function () {
			ReuseComponentSupport.mixInto(this, "component");
			// Defensive call of init of the super class:
			(UIComponent.prototype.init || jQuery.noop).apply(this, arguments);
		},

		setItemId: function (oExtensionAPI){
			const oNavigationController = oExtensionAPI.getNavigationController();
			const aKeys = oNavigationController.getCurrentKeys();
			const sItemId = aKeys[aKeys.length - 1];
			const oComponentModel = this.getComponentModel();
			oComponentModel.setProperty("/ItemId", sItemId);
		},

		/* Implementation of lifetime events specific for smart template components */
		/* Note that these methods are called because this component has been transformed into a reuse component */
		/* Check jsdoc of sap.suite.ui.generic.template.extensionAPI.ReuseComponentSupport for details */
		stStart: function (oModel, oBindingContext, oExtensionAPI) {
			const oComponentModel = this.getComponentModel();
			const oCanvasview = oComponentModel.getProperty("/View");
			const oPage = oCanvasview.byId("group");

			const oFCLActionButtons = oExtensionAPI.getFlexibleColumnLayoutActionButtons();
			oPage.addHeaderContent(oFCLActionButtons);
			this.setItemId(oExtensionAPI);
		},

		stRefresh: function (oModel, oBindingContext, oExtensionAPI) {
				this.setItemId(oExtensionAPI);
		}

		/* End of implementation of lifetime events specific for smart template components */
	});
});

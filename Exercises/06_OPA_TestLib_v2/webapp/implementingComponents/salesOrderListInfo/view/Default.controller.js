sap.ui.controller("STTASOWD20.implementingComponents.salesOrderListInfo.view.Default", {

	onInit: function () {
		const oComponent = this.getOwnerComponent();
		const oComponentModel = oComponent.getComponentModel();
		oComponentModel.setProperty("/View", this.getView());
	},

	onGenericInfo: function (){
		const oComponent = this.getOwnerComponent();
		const oComponentModel = oComponent.getComponentModel();
		const oExtensionAPI = oComponentModel.getProperty("/extensionAPI");
		const oNavigationController = oExtensionAPI.getNavigationController();
		const oNavigationData = {
			routeName: "salesOrderListInfo2genericInfoPage"
		};
		oNavigationController.navigateInternal("", oNavigationData);
	}
});

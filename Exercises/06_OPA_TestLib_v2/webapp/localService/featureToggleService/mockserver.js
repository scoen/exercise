sap.ui.define([
	"sap/ui/core/util/MockServer",
	"sap/base/Log"
], function (MockServer, Log) {
	"use strict";

	return {
		/**
		 * Initializes the mock server.
		 * You can configure the delay with the URL parameter "serverDelay".
		 * The local mock data in this folder is returned instead of the real data for testing.
		 * @public
		 */
		init: function (oOptions) {
			// create
			const oMockServer = new MockServer({
				rootUri: "/sap/opu/odata/sap/CA_FM_FEATURE_TOGGLE_STATUS_SRV/"
			});

			const sAppModulePath = "STTASOWD20/";
			const sMockdataPath = oOptions.mockdataPath || "/mockdataToggleOff";

			const sFeatureToggleMockserverPath = jQuery.sap.getModulePath(sAppModulePath) + "/localService/featureToggleService";
			oMockServer.simulate(sFeatureToggleMockserverPath + "/metadata.xml", {
				sMockdataBaseUrl: sFeatureToggleMockserverPath + sMockdataPath,
				bGenerateMissingMockData: true
			});

			oMockServer.start();

			Log.info("Running the app with mock data");
		}
    };
});

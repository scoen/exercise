sap.ui.define([
	"sap/ui/core/util/MockServer",
	"sap/ui/model/json/JSONModel",
	"sap/base/Log",
	"sap/base/util/UriParameters"
], function (MockServer, JSONModel, Log, UriParameters) {
	"use strict";
	let oMockServer;
	const _sAppModulePath = "STTASOWD20/";
	const _sJsonFilesModulePath = _sAppModulePath + "localService/C_STTA_SalesOrder_WD_20/mockdata";

	return {

		/**
		 * Initializes the mock server.
		 * You can configure the delay with the URL parameter "serverDelay".
		 * The local mock data in this folder is returned instead of the real data for testing.
		 * @public
		 */

		init: function () {
			return new Promise(function (fnResolve, fnReject) {
				const oUriParameters = jQuery.sap.getUriParameters();
				const sJsonFilesUrl = jQuery.sap.getModulePath(_sJsonFilesModulePath);
				const sManifestUrl = jQuery.sap.getModulePath(_sAppModulePath + "manifest", ".json");
				const oManifest = jQuery.sap.syncGetJSON(sManifestUrl).data;
				const oDataSource = oManifest["sap.app"].dataSources;
				const oMainDataSource = oDataSource.mainService;
				const sMetadataUrl = jQuery.sap.getModulePath(_sAppModulePath + oMainDataSource.settings.localUri.replace(".xml", ""), ".xml");
				// ensure there is a trailing slash
				const sMockServerUrl = /.*\/$/.test(oMainDataSource.uri) ? oMainDataSource.uri : oMainDataSource.uri + "/";
				const aAnnotations = oMainDataSource.settings.annotations;

				oMockServer = new MockServer({
					rootUri: sMockServerUrl
				});

				// configure mock server with a delay of 1s
				MockServer.config({
					autoRespond: true,
					autoRespondAfter: (oUriParameters.get("serverDelay") || 1000)
				});

				// load local mock data
				oMockServer.simulate(sMetadataUrl, {
					sMockdataBaseUrl: sJsonFilesUrl,
					bGenerateMissingMockData: true,
					aEntitySetsNames: ["C_STTA_SalesOrder_WD_20"]
				});

				// Assuming oMockServer is your mock server object
				const sRootUri = oMockServer.getRootUri();
				Object.keys(sap.ui.core.util.MockServer.HTTPMETHOD).forEach(function (sMethodName) {
					const sMethod = sap.ui.core.util.MockServer.HTTPMETHOD[sMethodName];
					oMockServer.attachBefore(sMethod, function (oEvent) {
						const oXhr = oEvent.getParameters().oXhr;
						const sUrl = decodeURIComponent(oXhr.url.replace(sRootUri, "/"));
						console.log("MockServer::before", sMethod, sUrl, oXhr.requestBody);
					});
					oMockServer.attachAfter(sMethod, function (oEvent) {
						const oXhr = oEvent.getParameters().oXhr;
						oXhr.onreadystatechange = function () {
							if (oXhr.readyState === 4) { // request has completed
								const sUrl = decodeURIComponent(oXhr.url.replace(sRootUri, "/"));
									let vResponse;
								try {
									vResponse = JSON.parse(oXhr.responseText);
								} catch (e) {
									vResponse = oXhr.responseText;
								}
								console.log("MockServer::after", sMethod, sUrl, vResponse);
							}
						};
					});
				});

				oMockServer.start();

				Log.info("Running the app with mock data");

				aAnnotations.forEach(function (sAnnotationName) {
					const oAnnotation = oDataSource[sAnnotationName];
						const sUri = oAnnotation.uri;
						const sLocalUri = jQuery.sap.getModulePath(_sAppModulePath + oAnnotation.settings.localUri.replace(".xml", ""), ".xml");

					//annotations
					new MockServer({
						rootUri: sUri,
						requests: [{
							method: "GET",
							path: new RegExp(MockServer.prototype._escapeStringForRegExp(sUri) + "([?#].*)?"),
							response: function (oXhr) {
								const oAnnotations = jQuery.sap.sjax({
									url: sLocalUri,
									dataType: "xml"
								}).data;

								oXhr.respondXML(200, {}, jQuery.sap.serializeXML(oAnnotations));
								return true;
							}
						}]

					}).start();

				});
				Log.info("Running the app with mock data");
				fnResolve();
			});
		},

		/**
		 * @public returns the mockserver of the app, should be used in integration tests
		 * @returns {sap.ui.core.util.MockServer}
		 */
		getMockServer: function () {
			return oMockServer;
		}
	};
});

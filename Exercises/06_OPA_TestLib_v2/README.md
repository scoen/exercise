# Test Fiori Elements & Feature Toggles with OPA5 Exercise

In this exercise the goal is to write OPA5 tests for an existing Fiori Elements application. In the second part, a feature toggle is introduced that should also be tested using QUnit and OPA5 tests.

> Remark: The structure of the Fiori Elements app is optimized to run with a local IDE. Therefore the FLP and mockserver setup may look different to what you know from the Web IDE templates.

## First Part: Use the FE test library

**Duration: 45 min**

1. Run the Fiori Elements app with mock data
1. Run the existing OPA5 tests
1. Replace existing tests by using the test library
   1. Import the test library as described on this [wiki page](https://wiki.wdf.sap.corp/wiki/x/cgZUaw)
   1. In `opaTests.qunit.html` add the following entry to `data-sap-ui-resourceroots`:
   `
   "sap.suite.ui.generic.template.integration.testLibrary" : "https://sapui5.hana.ondemand.com/test-resources/sap/suite/ui/generic/template/integration/testLibrary"
   `
   1. Replace the existing actions and assertions with suitable replacements
1. Test the extension by combining functions from the test library with your own page object functions.You could automate the following test scenario:
   1. Trigger the search
   1. Select an entry in the list report
   1. Click on the extension button "Custom Action" above the smart table
   1. Check that a dialog with title "Show Details" is displayed
   1. Check that the Sales Order ID matches the one from the selected entry. Add a custom action to identify the text in the popup.
   1. Close the popup
   1. Check that the smart table in the list report is displayed again
1. Run the tests:
    1. Open a terminal in the exercise root folder.
    1. Run `npm run start`.
    1. Open the OPA Test Suite.
    1. Alternatively use karma testrunner `npm run test:watch` in exercise root folder.

## Hints - Fiori Elements

- Use the UI5 Test Recorder to identify suitable actions and assertions
  - Press CTRL+ALT+SHIFT+T (SHIFT-CTRL-OPTION-T for Mac) to open the Test Recorder
  - Right click on UI element to generate control locator
  - Copy Snippet and use it in page object
  - Adapt the code snippet according to your scenario, e.g. replace a matcher
- The EntitySet can be found in the manifest json
- To click on the extension button you can use its ID (without the viewname) that can be found in the manifest.json

## Second Part: Test a feature toggle

**Duration: 45 min**

The goal of this exercise is to test a feature toggle via QUnit and OPA5 tests and to mock the feature maintenance API using a mockserver.

### Supporting Links

- [SAP Jam Page describing the feature maintenance cockpit (FMC)](https://jam4.sapjam.com/groups/EORJrSN9H35Xu8WAfOpYeX/overview_page/L2OkzHCaDKfvaI3JU1f9uT)
- [FMC itself](https://fmc.cfapps.sap.hana.ondemand.com/cp.portal/site#ownerFeature-Display?sap-ui-app-id-hint=s4hfthcpapp)
- [Feature Toggle Reuse Library](https://help.sap.com/viewer/6118ea8531f249f78272d0ff3a36e311/2011.500/en-US/5f54af458fc04402b1ea1df037930e86.html)

## Steps

1. Introduce a feature toggle using test driven development
   1. In `ext/controller/ListReportExtension.controller`, add `STTASOWD20/ext/service/FeatureToggleService` to the `sap.ui.define`. Have a look what the service is doing.
   1. Add an `onInit` function at the beginning of the controller that loads the feature toggles via the `featureToggleService`:

      ```javascript
         onInit: function () {
            const oFeatureToggleModel = this.getOwnerComponent().getModel("featureToggle");
            FeatureToggleService.loadFeatureToggles(oFeatureToggleModel);
         },
      ```

   1. You can check the oData model `featureToggle` in the `manifest.json` along with the corresponding dataSource `featureToggleService`
   1. Add a QUnit test for the function `useAToggle` in the `ListReportExtension.controller`. A QUnit setup is already there. Decide what should happen in case the feature toggle `dummyFeatureActive` is active. Use `sinon` to stub the `featureToggleService`.
   1. Implement the desired behaviour in the `useAToggle` function.
   1. Add a second test to test the behaviour if the toggle is inactive and add the respective productive code.
1. Add a mockserver setup for the feature maintenance cockpit API and use a feature toggle.
   1. Check out the already existing mockserver setup for the `featureToggleService` datasource in `localService/featureToggleService`. Note that there are 2 datasets for the specific toggle `"NewFragmentName"` being turned on or off respectively.
   Remember: Toggle active means it is not returned by the feature management cockpit API.
   1. Add the initialization for the `featureToggleService` to the `initFlpSandboxMockServer.js`:

      ```javascript
          aInitializations.push(featureToggleMockServer.init({mockdataPath: "/mockdataToggleOff"}));
      ```

   1. Start the application via `npm start`. The mockdata that is returned is dependent on the `mockdataPath` defined in the initialization script above.
   1. In the function `onCustomAction` in the `ListReportExtension.controller`, alter the used fragment by changing the variable `sFragmentName` depending on the toggle status:

      ```javascript
            const sFragmentName = FeatureToggleService.getFeatureStatus("NewFragmentName") ? "STTASOWD20.ext.fragments.NewDetails" : "STTASOWD20.ext.fragments.Details";
      ```

   1. Change the initialization for the `featureToggleService` in `initFlpSandboxMockServer.js` to `{mockdataPath: "/mockdataToggleOn"}`. Refresh the page and check the different popups by clicking on `Custom Action`.
1. Add an OPA5 Journey for testing the app with activated feature toggle.
   1. Add a second journey and add it to the `AllJourneys.js`. For the startup, use the following snipped to load the mockdata with activated feature toggle:

      ```javascript
            Given.iStartMyFLPApp({intent: "C_STTA_SalesOrder_WD_20-display", mockdataPath: "/mockdataToggleOn"});
      ```

   1. Add the initialization of the `featureToggleService` to the `Startup.js`:

      ```javascript
            aInitializations.push(featureToggleMockserver.init(oOptions));
      ```

   1. In `FLP.js`, in the function `iLeaveMyFLPApp`, add the destroy command to the success part:

      ```javascript
            sap.ui.core.util.MockServer.destroyAll();
      ```

      This makes sure the mockserver starts with fresh data for the next test.
   1. Add a test that checks for the changed details dialog title.
   1. Run the tests to check that both journeys (toggle off and toggle on) are working.

## Hints - Feature Toggles

- You can see the request and the mockdata that is returned for the `featureToggleService` in the network tab of your browser under `ToggleStatusSet.json`.

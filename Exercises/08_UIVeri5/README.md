# Exercise - Scenario and System-Test with UIVeri5

**Duration:** 60 minutes

**Deployed Shopping Cart on internal CF account:**

<https://uiveri5demo.internal.cfapps.sap.hana.ondemand.com/index.html>

> User: uiveri5demo@mailinator.com  
> PW: UIveri5demo

## Prerequisites

1. Install UIVeri5 like described in the [preparation guide](../../README.md) and update to latest Google Chrome Version (required for test execution). The complete installation & upgrade guide for UIVeri5 can be found [here](https://github.com/SAP/ui5-uiveri5).
1. Install Yeoman and generator-easy-ui5 if not done already. Run the following command below:

  ``` shell
    npm install -g yo generator-easy-ui5
  ```

## Generate the scaffolding with yeoman

1. In Visual Studio Code, open a new terminal and navigate to `Exercises/08_UIVeri5`.
1. Use the command `yo easy-ui5 project uiveri5` to call the Yeoman generator for UIVeri5 tests.
   This can take some minutes.
1. Answer the questions asked that helps the generator to create test structure:
1. Name of the project **uiveri5**
1. URL of the application: Copy the [link for the shoppingcart app](https://uiveri5demo.internal.cfapps.sap.hana.ondemand.com/index.html) from the browser.  
1. Type of authentication: Choose **SAP Cloud Platform**.
1. As an additional reporter, select `JUNIT` by pressing `space` on the corresponding line.  
   (Note: by default screenshot reporter is created while test execution)
1. To create page object, choose **Y** or **yes**
1. To add a spec file, choose **Y** or **yes**
1. Name of the page object: `ProductCatalog`
1. Skip action and assertion now.
1. Name for the suite: ``cart`` (that will be the name of the spec file).
1. Name for the spec (it block): ``I should see product category``
1. Press ``Enter``. Now the project structure will be generated and `npm install` will be executed.

## Configuration

Define the *authentication credentials* by adjusting the ``auth`` block in the [``conf.js``](./uiveri5/conf.js):

- Add the properties `userFieldSelector`, `passFieldSelector` and `logonButtonSelector` to the `sapcloud-form` block.
- Use the Inspector of your browser (`F12` in Chrome) to find the css selectors for the username input field, the password input field and the logon button in the DOM of the login page.  
  They look something like this: `userFieldSelector: 'input[name="..."]'`
- Your `auth` block may then looks like this:

  ```javascript
  auth: {
    'sapcloud-form': {
        userFieldSelector: '<CSS selector of user input field>',
        passFieldSelector: '<CSS selector of password input field>',
        logonButtonSelector: '<CSS selector of logon button>',
        user: '<user>',
        pass: '<pass>'
      }
  }
  ```

- Use the placeholder approach for `user` and `pass` to avoid storing the credentials in your config file, e.g.:  
    `user: '${params.placeholder}',`
- With that, you can execute your tests using a command like this in the `uiveri5` folder:  
    `uiveri5 --params.<placeholder1>="placeholder1Value" --params.<placeholder2>="placeholder2Value"`
- You can find the values for user and password at the top of the readme.

### Chromedriver configuration for Chinese colleagues

If you are located in China you have to configure and download the chromedriver manually:

1. Check your Google Chrome version (Go to the Settings and click "About Chrome")
1. Download the chromedriver that fits your Google Chrome version locally:
https://chromedriver.storage.googleapis.com/index.html
1. In UIVeri5 conf.js file define a localPath that refers to the executable chromedriver:

```javascript
connectionConfigs: {
   direct: {
      binaries: {
       chromedriver: {
          localPath: 'C:/chromedriver.exe'
        }
      }
   }
```

## Write the tests

- Open the `cart.spec.js` and write the first UIVeri5 test for the app *Shopping Cart*.  
- In your tests, use the generated page object. The syntax looks like this:

```javascript
it("I should see product category", function () {
    // add actions and assertions here
    When.onTheProductCatalogPage.<actionInCatalogPage>();
    Then.onTheProductCatalogPage.<assertionInCatalogPage>();
});
```

- Below you find an example test scenario you can execute.
- Use the UI5 testrecorder to find the actions and assertions for your page objects.
- [Execute your tests](#execute-uiveri5-tests) from time to time.
- If you need a new page object, you may use the [yeoman sub-generator for page objects](https://blogs.sap.com/2020/11/30/yeoman-generators-for-opa5-and-uiveri5/) (to be executed in the `08_UIVeri5` folder): `yo easy-ui5 project newuiveri5po`

### Test scenario

1. Click on the Categories "Printers".
1. Verify that the printers category was selected (e.g. by asserting on the page title)
1. Select one product e.g. "Laser Professional Eco" from left view.
1. Click on "Add to Cart button".
1. Verify that the product is displayed on the Product Detail View.
1. Click on "Show Shopping Cart" button.
1. Verify product is added in the shopping cart.

## Optional exercises

1. Pass the category key and the product id as **function parameters** to the related page object actions.
2. Provide a **common page object** that provides an action to press a button by passing an i18n-text. Use this common page object as **baseClass** for the existing page objects.
3. Run the UIVeri5 tests in Chrome headless mode

## Hints - UIVeri5

- With yeoman generator you can easily structure your tests with suitable page objects. You can use almost the same structure as in OPA5 with arrangements, actions and assertions.

- Explore the documentation in the [UIVeri5 GitHub](<https://github.com/SAP/ui5-uiveri5>) repository for further details about:

  - Assertions
    - [Jasmine Cheatsheet](https://devhints.io/jasmine) for **expect** syntax
    - [Using control properties](https://github.com/SAP/ui5-uiveri5/blob/master/docs/usage/expectations.md)
  - Configuration (define browser, define base url, pass user credentials, ...)

  - Page Objects

  - UI5 Locators

    - id (for stable IDs, avoid ID selectors using generated IDs, this is indicated by "__", e.g. "__button2")

      ```javascript
      var product = element(by.control({
       viewName: Viewname,
       id: "product"
      }));
      ```

    - properties

      ```javascript
      var addButton = element(by.control({
      controlType: "sap.m.Button",
          properties : {
              icon : "sap-icon://.."
          }
      })),
      
      ```

    - bindingPath

        ```javascript
        var product = element(by.control({
          controlType: "<Control Type>",
          bindingPath: {path: "<Paste content of binding path from UI5 test recorder>"}
       }));
      ```

    - i18NText

        ```javascript
        var addToCartButton = element(by.control({
          controlType: "<Your control type>",
          i18NText: { propertyName: "text", key: "<I18N-Text key" }
       }));
      ```

## Execute UIVeri5 tests

To pass the user credentials to the CLI:

- Commandline: `uiveri5 --params.<placeholder1>="placeholder1Value" --params.<placeholder2>="placeholder2Value"`
- Enable verbose mode: -v

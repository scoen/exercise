sap.ui.define(['sap/fe/test/ListReport', 'sap/ui/test/OpaBuilder', 'sap/ui/test/Opa5'], function (ListReport, OpaBuilder, Opa5) {
    'use strict';

    var AdditionalCustomListReportDefinition = {
        actions: {},
        assertions: {}
    };

    return new ListReport(
        {
            appId: 'sap.fe.demo.incidents',
            componentId: 'IncidentsList',
            entitySet: 'Incidents'
        },
        AdditionalCustomListReportDefinition
    );
});
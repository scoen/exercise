sap.ui.define(['sap/ui/test/opaQunit'], function (opaTest) {
  'use strict';

  var Journey = {
    run: function () {
      QUnit.module('Sample journey');

      opaTest("SAPUIVersion: " + sap.ui.version, function (Given, When, Then) {
        sap.ui.test.Opa5.assert.ok(true, sap.ui.version);
      });

      opaTest('#0: Start', function (Given, When, Then) {
        Given.iResetTestData().and.iStartMyApp("incidents-tile");
        Then.onTheMainPage.iSeeThisPage();
      });

      opaTest('#999: Tear down', function (Given, When, Then) {
        Given.iTearDownMyApp();
      });
    }
  };

  return Journey;
});
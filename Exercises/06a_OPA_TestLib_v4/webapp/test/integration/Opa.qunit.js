sap.ui.require(
    [
        "sap/fe/test/JourneyRunner",
        "sap/fe/test/Shell",
        "sap/fe/demo/incidents/test/integration/pages/MainListReport",
        "sap/fe/demo/incidents/test/integration/pages/MainObjectPage",
        "sap/fe/demo/incidents/test/integration/OpaJourney"
    ],
    function(JourneyRunner, Shell, MainListReport, MainObjectPage, Journey) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('sap/fe/demo/incidents') + '/index.html'
        });
        
        JourneyRunner.run(
            {
                pages: { onTheMainPage: MainListReport, onTheDetailPage: MainObjectPage }
            },
            Journey.run
        );
    }
);
# Gherkin Exercise

**Duration:** 45 minutes

**Hint:** If you get into problems, check the [troubleshooting section](../04_OPA/README.md#troubleshooting---common-mistakes).

## Steps

1. Navigate into your test/integration folder
2. Now create a new file: \<YourFeatureName\>.feature
3. The structure of this file should look like this:

```
Feature: <Title of your feature>

  Background:
    Given I start my App
    When on <pageName>: <I do something>
    When on <pageName>: <I do something>

  Scenario: <Scenario Name>
    Then on <pageName>: <I should see something>

  Scenario: <Second Feature Name>
    When on <pageName>: <I do something>

    Then on <pageName>: <I should see something>

 ```

4. Add the new file configureOpa.js file to the test/integration folder and reference all page objects you need for this feature:

```javascript
sap.ui.define([
  "sap/ui/test/Opa5",
  "./arrangements/Startup",
  // Load all needed Page Objects here
  <References to page objects>
  ], function (Opa5, Startup) {
  "use strict";

  Opa5.extendConfig({
    arrangements : new Startup(),
    viewNamespace : "sap.ui.demo.cart.view.",
    autoWait: true
  });
});
```

5. Add the file opaTests.qunit.js to the test/integration folder and reference the name of your feature file in the testHarness:
(Tip: You could also reference multiple features here.)

```javascript
/* global QUnit */

QUnit.config.autostart = false;

sap.ui.require([
  "sap/ui/test/gherkin/opa5TestHarness",
  "sap/ui/demo/cart/test/integration/configureOpa"
  ], function (testHarness) {
  "use strict";

  testHarness.test({
    featurePath: "<Reference to feature file>", 
    generateMissingSteps : true
  });

  QUnit.start();
});
```

6. Write the test:
   1. In your feature file, replace the placeholders with already existing page object names and actions and assertions from the page objects. You can choose by yourself what you want to test, but an example test scenario is listed [below](#test-scenario)
   1. Add more scenarios.

7.  Run the tests:
    1.  Open a terminal in the exercise root folder.
    2.  Open the project structure in your browser `ui5 serve -o`
    3.  Go into `webapp/test/integration` subfolder and open `opaTests.qunit.html`
    4.  Alternatively use karma testrunner `npm run test:watch` in exercise root folder.

### Test scenario

1. Start the app.
1. Press on the "Flat Screens" category.
1. Select one product.
1. Press on the "Add to Cart" button.
1. Press on the "Show Shopping Cart" button.
1. Press on "Save for Later".
1. Verify that there is one product in the save for later list.
1. Verify that the shopping cart is empty.

## Optional Exercise: Add a steps file

8. Add a steps.js in test/integration folder, e.g.:

```javascript
sap.ui.define([
  "sap/ui/test/gherkin/StepDefinitions",
  "sap/ui/test/Opa5",
  "sap/ui/test/actions/Press"
], function (StepDefinitions, Opa5, Press) {
  "use strict";

  return StepDefinitions.extend("GherkinWithOPA5.Steps", {
    init: function () {
      var oOpa5 = new Opa5();
      this.register(/^I press on (.*)$/i, function (sProductName) {
        oOpa5.waitFor({
          controlType: "sap.m.ObjectListItem",
          viewName: "Category",
          bindingPath: {
            path: `/Products('${sProductName}')`,
            propertyPath: "PictureUrl"
          },
          actions: new Press({
            idSuffix: "content"
          })
        });
      });
    }
  });
});
```

You may change the parameters of `this.register()` according to your needs. <br>
Be careful: You can overwrite the existing mappings to the page objects since OPA5 only generates them if there is no matching step definition!

9. Extend the opaTests.qunit.js by importing `configureOpa` and your new `steps.js` and adding it to the testHarness:

```javascript
sap.ui.require([
	"sap/ui/test/gherkin/opa5TestHarness",
	"sap/ui/demo/cart/test/integration/configureOpa",
	"sap/ui/demo/cart/test/integration/steps"
], function (testHarness, configureOpa, stepsFile) {
	"use strict";

	testHarness.test({
		featurePath: "<Reference to feature file>",
		generateMissingSteps : true,
		steps:  stepsFile
	});

	QUnit.start();
});
```

10. Adjust your .feature file to use the steps.js instead of a page object. In this example you can use the productname as a parameter:

```
Background:
    Given I start my App
    When on home: I press on "The Flat Screens category"
    When I press on HT-1254
```

11. You can also use the already defined page objects and provide them with parameters. To do that, add a second `this.register()` block to the `init` function your steps.js, e.g.:

```javascript
this.register(/^on the cartview I should see (.*?) product(?:s)? in my save for later list$/i,
        // Hint: Given and When are needed here even if they are not used. 
        // Otherwise "Then" can not be interpreted
        function(sValue, Given, When, Then) {
            Then.onTheCart.iShouldSeeProductsInMySaveForLaterList(sValue);
        });
```

12. Add the corresponding step to your .feature file, e.g.

```
Then on the cartview I should see 1 product in my save for later list
```
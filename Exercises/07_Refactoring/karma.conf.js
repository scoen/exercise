module.exports = function (config) {
  config.set({
    frameworks: ['qunit'],

    files: [
      { pattern: 'src/**/*.js', type: 'module' },
      { pattern: 'test/**/*.js', type: 'module' }
    ],

    client: {
      clearContext: false,
      qunit: {
        showUI: true,
        testTimeout: 5000
      }
    },

    browsers: ['ChromeHeadless'],
    browserConsoleLogOptions: {
      level: 'warn'
    },

    preprocessors: {
      'src/**/*.js': ['coverage']
    },

    coverageReporter: {
      includeAllSources: true,
      reporters: [
        {
          type: 'html',
          dir: './target/coverage'
        },
        {
          type: 'text'
        }
      ]
    },

    junitReporter: {
      outputDir: './target/junit',
      outputFile: 'TEST-qunit.xml',
      suite: '',
      useBrowserName: true
    },

    htmlReporter: {
      outputFile: './target/html/QUnit.html',

      // Optional
      pageTitle: 'Test Results',
      subPageTitle: 'Detailed test results',
      groupSuites: true,
      useCompactStyle: true,
      useLegacyStyle: true,
      showOnlyFailed: false
    },
    reporters: [
      'progress',
      'coverage',
      'junit',
      'html'
    ]
  })
}

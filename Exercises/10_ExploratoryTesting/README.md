# Exploratory Testing Exercise

1. Take five minutes to read through this short blogpost about [exploratory testing experiences](https://altom.com/testing-tours-add/).
2. Decide toghether with your pair partner what kind of tour you want to try out.
3. One of you is the driver and the other one should focus on taking notes

Take the time of 60 minutes to do multiple different tours through the shopping cart app!

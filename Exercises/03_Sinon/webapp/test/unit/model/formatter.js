sap.ui.define([
	'sap/ui/demo/cart/model/formatter',
	'sap/ui/demo/cart/model/CurrencyConverter',
	'sap/ui/thirdparty/sinon-4'
], function (formatter, CurrencyConverter, sinon) {
	"use strict";

	var sDefaultLanguage = sap.ui.getCore().getConfiguration().getLanguage();

	QUnit.module("formatter - statusText");

	QUnit.test("Should see statusText 'Available'", function (assert) {
		assert.equal(formatter.statusText('A'), "Available", "Status Text is correct");
	});

	QUnit.test("Should see statusText 'Out of Stock'", function (assert) {
		assert.equal(formatter.statusText('O'), "Out of Stock", "Status Text is correct");
	});

	QUnit.test("Should see statusText 'Discontinued'", function (assert) {
		assert.equal(formatter.statusText('D'), "Discontinued", "Status Text is correct");
	});

	QUnit.test("Should see statusText 'Other'", function (assert) {
		assert.equal(formatter.statusText('Other'), "Other", "Status Text is correct");
	});

	QUnit.module("formatter - price", (hooks) => {

		hooks.before(function() {
			sap.ui.getCore().getConfiguration().setLanguage("en-US");	
		});

		hooks.after(function() {			
			sap.ui.getCore().getConfiguration().setLanguage(sDefaultLanguage);
		});

		QUnit.test("Should have a price function", function (assert) {
			assert.ok(formatter.hasOwnProperty("price"), "There is a price function");
		});
		
		QUnit.test("Price should return error when parameter is not number", function(assert) {
			assert.equal(formatter.price("Text"), "Parameter is not a number", "Error is returned");	
		});
		
		QUnit.test("Price should return number with two digits after comma", function(assert) {
			assert.equal(formatter.price(123), "123.00", "Price is formatted with two digits");	
		});
		
		QUnit.test("Price should return number . as seperator for big numbers", function(assert) {
			assert.equal(formatter.price(12345), "12,345.00", "Price is formatted with two digits and dot");	
		});
	});
});
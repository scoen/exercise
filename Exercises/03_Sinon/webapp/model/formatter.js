sap.ui.define([
	"sap/ui/model/resource/ResourceModel",
	"sap/ui/core/format/NumberFormat",
	"sap/ui/demo/cart/model/CurrencyConverter"
], function (ResourceModel, NumberFormat, CurrencyConverter) {
	"use strict";
	
	var formatter = {
		
		/**
		 * Returns the status text based on the product status
		 * @param {string} sStatus product status
		 * @return {string} the corresponding text if found or the original value
		 */
		statusText: function (sStatus) {
			var oBundle = new ResourceModel({
				bundleUrl: sap.ui.require.toUrl("sap/ui/demo/cart/") + "i18n/i18n.properties"
			}).getResourceBundle();
			var mStatusText = {
				"A": oBundle.getText("statusA"),
				"O": oBundle.getText("statusO"),
				"D": oBundle.getText("statusD")
			};

			return mStatusText[sStatus] || sStatus;
		},
		
		price: function (number){
		    if(!isNaN(number)) {
		    	var numberFormat = NumberFormat.getFloatInstance({
		    		decimals: 2,
		    		decimalSeparator: ".",
		    		groupingSeparator: ","
		    	});
		    	return numberFormat.format(number);
		    }
		    else {
		    	return "Parameter is not a number"
		    }
	    },
	    
	   /**
		 * Returns the price in a specific currency as text, e.g. "123.00 INR"
		 * @param {float} number that should converted in currency
		 * @param {string} currency that number should be converted to
		 * @return {string} the converted number
		 */
		convertNumberIntoCurrency: function (number, currency) {
			var price = this.price(number);
			if ((price !== "Parameter is not a number") && currency) {
				return CurrencyConverter.getPriceInCurrency(price, currency) + " " + currency;
			} else {
				return "Cannot convert number"
			}
		},

		convertedPrice: "",

		/**
		 * Function needed for optional excercise.
		 * Saves the price in a specific currency as text, e.g. "123.00 INR" to convertedPrice after a specified timeout.
		 * @param {float} number that should converted in currency
		 * @param {string} currency that number should be converted to
		 * @param {int} waitingTimeMillis milliseconds the timeout should wait until convertNumberIntoCurrency is called
		 */
		waitForCurrencyConversion: function(number, currency, waitingTimeMillis) {
			setTimeout(() => {
				this.convertedPrice = this.convertNumberIntoCurrency(number, currency);
			}, waitingTimeMillis);
		}
	};
	
	return formatter;
});

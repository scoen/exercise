# Exercise - Unit-Tests with QUnit and Sinon

**Duration:** 60 minutes

## Instructions

Write QUnit tests for the app *Shopping Cart* to convert a number and return it as price in a specific currency. Use sinon to isolate your tests from a Currency Converter that is not implemented.

1. Go to the **Exercises/03_SINON** folder.
2. Create QUnit tests for formatter.convertNumberIntoCurrency and isolate your tests from the Currency Converter (that is not implemented yet).
3. Define a sandbox object to restore all test doubles after each test run. See hints for more details.
4. Write tests that cover the following aspects:
   - Use a **spy** to check that the function `formatter.price` is **called with the right parameter** and is **called exactly once** in our function under test `convertNumberIntoCurrency`. See hints for more details.
   - Use a **stub** to check that the Currency Converter function `getPriceInCurrency` is **called with the right parameter** and is **called once** in our function under test `convertNumberIntoCurrency`?
   - Use a **stub** to check that the Currency Converter is **called not at all** if you pass NaN or an undefined currency to it in our function under test `convertNumberIntoCurrency`?
   - Verify that `formatter.convertNumberIntoCurrency` **returns the converted price with currency** by stubbing the return value of the Currency Converter, e.g. "9000 INR"
   - Verify that `formatter.convertNumberIntoCurrency` **returns the converted price with currency** by returning different stub values for the first and the second call of the Currency Converter.
5. Run the tests:
    1. Open a terminal in the exercise root folder.
    2. Run `npm run start`
    3. Open the Unit Test Suite.
    4. Alternatively use karma testrunner `npm run test:watch` in exercise root folder.

## Optional exercises

1. Add a test that verifies that the function `formatter.waitForCurrencyConversion` saves the converted currency to a variable in the `formatter` object.  
Use a mock to specify the behavior of the function `CurrencyConverter.getPriceInCurrency` and [Sinon Fake Timers](https://sinonjs.org/releases/v1.17.6/fake-timers/) to simulate the timeout.

## Hints - QUnits

- Use the sinon sandbox to restore all stubs after each test
- How to create a sinon sandbox:
  
  ```javascript
  var sandbox;
  ...
  QUnit.module(<Name of your module>, (hooks) => {

	   hooks.before(function () {
			sandbox = sinon.sandbox.create();
		}),

		hooks.afterEach(function () {
			sandbox.restore();
		}),
  ```

- How to define a spy for a function

  ```javascript
  var spy = sandbox.spy(<Object>, "<Name of the function you want to spy on>");
  ```

- How to define a stub for a function

  ```javascript
  var stub = sandbox.stub(<Object>, "<Name of the stubbed function>").returns(<return value>);
  ```

- How to define a mock for an object
  
  ```javascript
  var mock = sandbox.mock(<Object>);
  mock.expects("<Name of the fuction you want to mock>").withArgs(<Argument1>, <Argument2>).returns(<return value>);
  <Call to function under test>
  mock.verify();
  ```

- How to define an assert for a spy/stub

  ```javascript
  assert.ok(spy.calledWith(...), "function was called with passed number");
  assert.ok(spy.calledOnce, "function was called once");
  ```

- How to use [Sinon Fake Timers](https://sinonjs.org/releases/v1.17.6/fake-timers/)

  ```javascript
  var clock = sinon.useFakeTimers();
  clock.tick(<timeInMilliseconds>);
  ```

- Use the [sinon documentation](https://sinonjs.org/)
  > UI5 uses an older version of sinon.js, therefore you cannot use all features of it

## Troubleshooting - Common Mistakes

- If you face an issue with missing plugins during the startup of karma enter `npm link <name of the plugin>` in the terminal, e.g. `npm link karma-webdriver-launcher`.

# Exercise - Run your App with the Mock Server V4

**Duration:** 45 minutes

**Hint:** check the offical [mockserver documentation](https://github.tools.sap/ux/mockserver).

## Instructions

1. Add the mockserver as dev-dependency to your project
``` shell
npm install @sap/ux-ui5-fe-mockserver-middleware --save-dev
```
2. Add the dependency to the ui5 array in the **package.json**
``` javascript
"ui5": {
    "dependencies": [
      "@sap/ux-ui5-fe-mockserver-middleware"
    ]
}
```
3. Add the configuration for the mockserver to the `ui5.yaml` file. Replace the placeholders in `urlBasePath`, `metadataXmlPath` and `mockdataRootPath` with suitable values for the app under test.
``` yaml
server:
  customMiddleware:
    - name: sap-fe-mockserver
      beforeMiddleware: compression
      configuration:
        service:
          urlBasePath: "/path/to/service"
          name: ""
          metadataXmlPath: "./path/to/metadata.xml"
          mockdataRootPath: "./path/to/mockdata"
          generateMockData: true
```
**Hint:** you can find the serviceUrl of your ODataService in the `manifest.json` under the object `mainService/uri`

4. Start the application in the terminal with following command and check that the mockdata is provided:
``` shell
npm run start
```

5. In the toolbar of the table is a button called `Change Title`. This button triggers a title change on the selected entry, via an action. Check with the help of the `metadata.xml` to which entity the action belongs and create a `.js` file with the corresponding entitySet name in the `./localService/mockdata` folder, to mock that action.

**Hint:** First search for the name `Change Title` in the `metadata.xml` to identify the action that is bound to the button. Then search for the action name to identify the entityType that is bound to the action.

6. Add the following snippet to the created `.js` file to add the handler for mocking actions
``` javascript
module.exports = {
    executeAction: function (actionDefinition, actionData, keys) {
        console.log('Action called for ' + JSON.stringify(keys));
    }
};
```
7. Restart the server and execute the `Change Title` action in the toolbar of the List Report. Check in the console log in the terminal in VS Code, that the action was called.

8. Start a `JavaScript Debug Terminal` in VSCode, add a Breakpoint to the `executeAction` function and start the application in the Debug Terminal. 
9. Click the `Change Title` button again and switch to VS Code. The breakpoint is triggered and you can debug the parameters.  
**Hint**: You can add parameters to the Watch view of the debug perspective. 
10. Write an "if-statement" that checks if the correct action is triggered.
11. If the correct action is triggered identify the selected entry and update the title of the selected entry. Use the help of the [MockServer API](https://github.tools.sap/ux/mockserver/blob/main/documentation/Mock%20Data%20File%20API.md) 
  - `const aAllEntries = this.base.getAllEntries()` will return all entries that are available for that entity
  - To identify our selected entry we use the javascript function [find](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/find)
  - `const oSelectedEntry = aAllEntries.find((oEntry) => oEntry.ID === keys.ID)`
  - rename the title of your selected Entry
  - update the entry with the changed title

**Hint:** `this.base.updateEntry(key, updatedEntry)` needs the whole updated entry passed, otherwise everything else from the entry will be removed.

12. Restart the server again and check that the button renames the title.

# OPA Advanced Exercise

In this exercise you are going to continue building OPA test for the shopping cart application.
You can extend the started Navigation Journey, as well as create your own journeys for the app.

This is a creative exercise where you can write and design your very own OPA tests. Therefore, we will not give you concrete exercise steps. You will only get some tips & tricks and help from the trainers.

**Hint:** If you get into problems, check the [troubleshooting section](../04_OPA/README.md#troubleshooting---common-mistakes).

## Tips & Tricks

1. Take a few minutes to click through the app and decide for a journey you want to design.

2. Possible journeys you could try to build are: Compare two products, delete a product, buy a product, enhance the navigation journey, etc.

3. You may need multiple new page objects.

4. You may need new journey files.

5. Documentation on matchers can be found [here](https://sapui5.hana.ondemand.com/#/api/sap.ui.test.matchers).

6. An iStartMyApp does always need a Teardown, but not necessarily in the same opaTest. After a new iStartMyApp you will end up with an app opened on the entry page again. If you don't want that, you could handover an app url as parameter to the iStartMyApp function.

7. A lot of different test examples can be found [here](https://sapui5.hana.ondemand.com/#/topic/1b47457cbe4941ee926317d827517acb) (Steps 6-13).

/* global QUnit */

sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/Home",
	"./pages/Category",
	"./pages/Product",
	"./pages/Welcome"
], function (opaTest) {
	"use strict";

	QUnit.module("Navigation Journey");

	opaTest("Should start the app and go to the speaker category view", function (Given, When, Then) {
		// Arrangements
		Given.iStartMyApp();
		// Actions
		When.onHome.iPressOnTheSpeakerCategory();
		// Assertions
		Then.onTheCategory.iShouldBeTakenToTheSpeakerCategory();
	});

	opaTest("Should see the product Blaster Extreme", function (Given, When, Then) {
		// Actions
		When.onTheCategory.iPressOnTheProductBlasterExtreme();
		// Assertions
		Then.onTheProduct.iShouldSeeTheBlasterExtremeDetailPage();
	});

	opaTest("Should navigate back to home", function (Given, When, Then) {
		// Actions
		When.onTheCategory.iPressTheBackButtonInCategory();
		// Assertions
		Then.onHome.iShouldSeeTheCategoryList();
		Then.onTheWelcomePage.iShouldSeeTheWelcomePage();
		// Cleanup
		Then.iTeardownMyApp();
	});

});

# Exercise - Test Driven Development with Javascript and QUnit

**Duration:** 45 minutes

## Instructions

Develop a feature to format the price of a product using test-driven development.

* Go to the **Exercises/02_TDD** folder.
* Implement the following requirements step by step in a test-driven way.
   1. Add a test to `test/unit/model/formatter` that asserts that the function call `formatter.price(42)` returns `"42.00"`. Run the tests to see they are failing.
   1. Add the function `price` to the formatter and implement the minimal possible functionality to make the test pass. Remember to keep it simple and stupid (KISS).
   1. Add a second test which asserts that `formatter.price(21)` returns `"21.00"`. Don't change the existing tests. Check that the test is failing.
   1. Add only the minimal necessary code to make all tests pass.
   1. Add a new test that asserts that `formatter.price` returns an error text in case the provided parameter is not a number.
   1. Add the minimal necessary functionality to make the test pass. Refactor your productive code if necessary.
   1. Go on in implementing the following requirements using test-driven development. <br>
     As before, always write the test first and only add as much fuctionality as is needed to make the tests pass. While the test are green, you may refactor your productive code.
   1. `formatter.price(42.21)` should return `"42.21"`. (if you like you can use [sap.ui.core.format.NumberFormat.getFloatInstance](https://sapui5.hana.ondemand.com/#/api/sap.ui.core.format.NumberFormat)).
   1. `formatter.price(1042)` should return `"1,042.00"`.

## Optional Exercise

* Think of new requirements the formatter should fulfill and implement them in test-driven development.

## Hints

* By executing `npm run test:watch` in the exercise root folder, you can use the karma test runner to keep your tests watched.
* Structure the test for `formatter.price` in a new `QUnit.module` to keep the overview.
* In the SAPUI5 SDK you get all necessary details about [**sap.ui.core.format.NumberFormat**](https://sapui5.hana.ondemand.com/#/api/sap.ui.core.format.NumberFormat)

## Troubleshooting - Common Mistakes

- If you face an issue with missing plugins during the startup of karma enter `npm link <name of the plugin>` in the terminal, e.g. `npm link karma-webdriver-launcher`.

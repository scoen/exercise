sap.ui.define([
	"sap/ui/demo/cart/model/formatter"
], function (formatter) {
	"use strict";

	QUnit.module("formatter - statusText", (hooks) => {

		QUnit.test("Should see statusText 'Available'", function (assert) {
			assert.equal(formatter.statusText("A"), "Available", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Out of Stock'", function (assert) {
			assert.equal(formatter.statusText("O"), "Out of Stock", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Discontinued'", function (assert) {
			assert.equal(formatter.statusText("D"), "Discontinued", "Status Text is correct");
		});

		QUnit.test("Should see statusText 'Other'", function (assert) {
			assert.equal(formatter.statusText("Other"), "Other", "Status Text is correct");
		});

	});
});

# Exercise - Unit-Tests with QUnit

**Duration:** 50 minutes

## Prerequisites

- Clone this [repository](../../../..) into your local IDE workspace.

## Instructions

Write first *QUnit* tests for the app *Shopping Cart* to format the status text of a product.
1. Go to the **Exercises/01_QUNIT** folder.
2. Create a testsuite in webapp/test/unit/
   > Due to CSP compliance you should create an html and a js-file.
   > Create the file **webapp/test/unit/unitTests.qunit.html** and add the following content:

   ```html
   <!DOCTYPE html>
   <html>
      <head>
         <title><Title of your test suite></title>
         <meta charset="UTF-8">

         <script
            id="sap-ui-bootstrap"
            src="https://sapui5.hana.ondemand.com/resources/sap-ui-core.js"
            data-sap-ui-resourceroots='{
               "sap.ui.demo.cart": "../../"
            }'
            data-sap-ui-async="true">
         </script>
         <link rel="stylesheet" type="text/css" href="https://sapui5.hana.ondemand.com/resources/sap/ui/thirdparty/qunit-2.css">

         <script src="https://sapui5.hana.ondemand.com/resources/sap/ui/thirdparty/qunit-2.js"></script>

         <script src="unitTests.qunit.js"></script>

      </head>
      <body>
         <div id="qunit"></div>
         <div id="qunit-fixture"></div>
      </body>
   </html>
   ```

   > Create the file **webapp/test/unit/unitTests.qunit.js** and replace the place holder. The `AllTests.js` will be created in step 8.

   ```javascript
   QUnit.config.autostart = false;

   sap.ui.getCore().attachInit(function () {
      "use strict";

      sap.ui.require([
         "<Refer to AllTests>"
      ], function () {
         QUnit.start();
      });
   });

   ```

3. Create a subfolder for your formatter tests. Your test structure should match the app structure to easily find the corresponding unit tests.
4. Create the new formatter QUnit Test.
5. In the test, refer to the formatter class in your productive code. <br>
Hint: Use the fullqualified path to the formatter class (starting with `sap/ui/demo/cart`)

   ```javascript
   sap.ui.define([<Refer to formatter class>], function(<alias>) { });
   ```

6. Select a suitable module name for your tests and  write your first test that challenges the formatter.statusText.

   ```javascript
   QUnit.module("<Module name>", () => {
      
      QUnit.test("Should Test Description", function(assert) {
        assert.ok(false, "Implement me");
      });

   });
   ```

7. Create the **AllTests.js** file and refer the new QUnit Test:
   
    ```javascript
   sap.ui.define([<Reference to QUnit formatter>], function () {
      "use strict";
   });
   ```

8.  Run the tests:
    1. Open a terminal in the exercise root folder.
    1. Open the project structure in your browser by running `ui5 serve -o`
    1. Go into `webapp/test/unit` subfolder and open `unitTests.qunit.html`
    1. Alternatively use the script `npm start` and open `unitTests.qunit.html`
    1. You may also use the karma testrunner `npm run test:watch` in the exercise root folder.

9. Add more tests. Check the coverage report to see if you covered all the lines and branches.

10. Use eslint and pre-commit hooks:
    1. In the terminal, run `npm run eslint`. See if any errors or warnings are reported.
    1. Fix the errors and warnings. If you have the eslint plugin for VS Code installed, try using the shortcut `ctrl + .` for quick fixes.
    1. Check the [package.json in the root directory of the repository](../../package.json) for the `"ghooks"` configuration. [Ghooks](https://github.com/ghooks-org/ghooks) is a plugin that automatically sets up git hooks, e.g. the `pre-commit hook` that is executed before `git commit`.
    1. Add a script similar to `lint-test-qunit-solution` in the ghooks `pre-commit` hook to automatically run the test in the qunit exercise while committing. If you use `npm test` this will also check on the coverage thresholds defined in the `karma.conf.js`.
    1. Commit your changes to see it working.

## Hints

- Your test suite should be named according the following convention: unitTests.qunit.html / unitTests.qunit.js / AllTests.js
- You can find all status texts in the i18n.properties file
- How many tests cases are needed to cover all [equivalence classes](https://www.testingexcellence.com/equivalence-partitioning/)?


## Optional Exercise

11. Make yourself familiar with different assertions. You can find them in the [QUnit Documentation](https://api.qunitjs.com/assert/).

## Troubleshooting - Common Mistakes

- If you face an issue with missing plugins during the startup of karma enter `npm link <name of the plugin>` in the terminal, e.g. `npm link karma-webdriver-launcher`.

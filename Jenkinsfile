@Library('piper-lib-os') _

node {

    properties([pipelineTriggers([cron('0 1 * * *')])])

    stage('Prepare Pipeline') {
        checkout scm
        setupCommonPipelineEnvironment script:this
    }

    stage('Karma Tests') {
        karmaExecuteTests script: this
    }

    stage('System Tests') {
        withCredentials([usernamePassword(
            credentialsId: 'uiveri5tests',
            passwordVariable: 'password',
            usernameVariable: 'username'
        )]) {
            uiVeri5ExecuteTests script: this,
            runOptions: [
                "--params.user=${username}",
                "--params.pass=${password}",
                '--seleniumAddress=http://localhost:4444/wd/hub',
                '--specs=./Solutions/08_UIVeri5/cart.spec.js',
                './Solutions/08_UIVeri5/conf.js'
            ]
        }

        archiveArtifacts allowEmptyArchive: true, artifacts: '**/target/report/screenshots/**'

        publishHTML target: [
            allowMissing: true,
            alwaysLinkToLastBuild: true,
            keepAll: true,
            reportDir: 'target/report/screenshots/',
            reportFiles: 'report.html',
            reportName: 'UIVeri5 Test Report'
        ]
    }

    stage('Publish Results') {
        testsPublishResults script: this
        cobertura coberturaReportFile: '**/target/coverage/cobertura/cobertura-coverage.xml', enableNewApi: true
    }
}
